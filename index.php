<!DOCTYPE html>
<html ng-app="Prueba">
	<head lang="es">
		<!--Meta-->
		<title>Dashboard | Evaluación Psicológica</title>
		<meta name="author" content="Gabriel Valero" />
		<meta name="description" content="Administración Evaluación Psicológica">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<base href="/EP-Dashboard/">
		<!--<base href="/dashboard/">-->


		<!--Favicon-->
		<link rel="icon" href="favicon2.ico">


		<!--Libs styles sheet-->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="assets/css/animate.css"/>
		<link rel="stylesheet" type="text/css" href="assets/libs/toaster/toaster.css">
		<link rel="stylesheet" type="text/css" href="assets/libs/sweet-alert/sweetalert2.css">
		<link rel="stylesheet" type="text/css" href="assets/libs/calendar/fullcalendar.css" />
		<link rel="stylesheet" type="text/css" href="assets/libs/kendo-calendar/kendo.common-material.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/libs/kendo-calendar/kendo.material.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/libs/kendo-calendar/kendo.material.mobile.min.css" />
		<link rel="stylesheet" type="text/css" href="assets/libs/jquery-clockpicker/jquery-clockpicker.css" />
		<link rel="stylesheet" type="text/css" href="assets/libs/angular-complete/angular-complete.css" />
		<link rel="stylesheet" type="text/css" href="assets/libs/angular-switch/angular-ui-switch.css" />
		<link rel="stylesheet" type="text/css" href="assets/libs/rzslider/rzslider.min.css"/>
		<link rel="stylesheet" type="text/css" href="assets/libs/angular-minicolors/jquery.minicolors.css"/>
		<!--<link rel="stylesheet" type="text/css" href="assets/css/hexagons.min.css">-->
		<link rel="stylesheet" type="text/css" href="assets/css/template/neon.css"/>
		<link rel="stylesheet" type="text/css" href="assets/css/template/styles-ie.css"/>
		<link rel="stylesheet" type="text/css" href="assets/css/template/styles-responsive-ie.css"/>


		<!-- Less stylesheet -->
		<link rel="stylesheet/less" type="text/css" href="assets/less/styles.less"/>


		<!--Fonts-->
		<link rel="stylesheet" type="text/css" href="assets/fonts/icons/css/icons.css">
		<link rel="stylesheet" type="text/css" href="assets/css/template/entypo.css">
		<link rel="stylesheet" type="text/css" href="assets/css/template/font-awesome-custom.min.css">
		<link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

		<!--Jquery Lib-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="assets/libs/JQuery/jquery-2.1.4.min.js"><\/script>')</script>


		<!--IE script-->
		<script>
			var isIE = /*@cc_on!@*/false || !!document.documentMode;
			if (isIE) {
				location.href = "https://www.google.es/chrome/browser/desktop/index.html";
			}
		</script>
	</head>



	<body class="page-body" idle
		  ng-class="{'animations': configuration.animations,
					 'no-animations': !configuration.animations,
					 'theme_alternate': !configuration.theme_basic,
					 'theme_basic': configuration.theme_basic,
					 'chrome': isChrome(),
					 'firefox': isFirefox()
				 }">


		<!--APP SPLASH-->
		<div class="splash" ng-cloak="" ng-if="splash">
			<div class="container-splash">
				<div class="ball"></div>
				<div class="ball1"></div>
			</div>
		</div>


		<!--APP-->
		<div>
			<toaster-container ></toaster-container>
			<div id="app-view" ui-view="app-view"></div>
		</div>




		<!-- Angular Libs Script -->
		<script type="application/javascript" src="assets/libs/angular-less/prefixfree.min.js"></script>
		<script type="application/javascript" src="assets/libs/angular-less/less.js"></script>
		<script type="application/javascript" src="assets/libs/angular/angular.min.js"></script>
		<script type="application/javascript" src="assets/libs/angular-ui-router/angular-ui-router.min.js"></script>
		<script type="application/javascript" src="assets/libs/angular-less/angular.less.js"></script>
		<script type="application/javascript" src="assets/libs/angular-cookies/angular-cookies.js"></script>
		<script type="application/javascript" src="assets/libs/angular-crypto/crypto-aes.js"></script>
		<script type="application/javascript" src="assets/libs/angular-animate/angular-animate.js"></script>
		<script type="application/javascript" src="assets/libs/ng-idle/ng-idle.js"></script>
		<script type="application/javascript" src="assets/libs/toaster/toaster.js"></script>
		<script type="application/javascript" src="assets/libs/angular-text-truncate/angular-text-truncate.js"></script>
		<script type="application/javascript" src="assets/libs/angular-complete/angular-complete.min.js"></script>


		<!-- App Script -->
		<script type="application/javascript" src="app/app/app.module.js"></script>
		<script type="application/javascript" src="app/app/app.const.js"></script>
		<script type="application/javascript" src="app/app/app.config.js"></script>
		<script type="application/javascript" src="app/app/app.factory.js"></script>
		<script type="application/javascript" src="app/app/app.directive.js"></script>
		<script type="application/javascript" src="app/app/app.filter.js"></script>


		<!-- App Controllers -->
		<script type="application/javascript" src="app/app/app.controller.js"></script>
		<script type="application/javascript" src="app/app/app.auxiliar.controller.js"></script>
		<script type="application/javascript" src="app/app/app.idle.controller.js"></script>
		<script type="application/javascript" src="app/app/app.crud.controller.js"></script>
		<script type="application/javascript" src="app/login/login.controller.js"></script>
		<script type="application/javascript" src="app/dashboard/dashboard.controller.js"></script>
		<script type="application/javascript" src="app/profile/profile.controller.js"></script>
		<script type="application/javascript" src="app/tests/tests.controller.js"></script>
		<script type="application/javascript" src="app/tests/tests-item.controller.js"></script>
		<script type="application/javascript" src="app/faqs/faqs.controller.js"></script>
		<script type="application/javascript" src="app/faqs/faqs-item.controller.js"></script>
		<script type="application/javascript" src="app/documents/documents.controller.js"></script>
		<script type="application/javascript" src="app/documents/documents-item.controller.js"></script>
		<script type="application/javascript" src="app/teachings/teachings.controller.js"></script>
		<script type="application/javascript" src="app/teachings/teachings-item.controller.js"></script>
		<script type="application/javascript" src="app/cases/cases.controller.js"></script>
		<script type="application/javascript" src="app/cases/cases-item.controller.js"></script>
		<script type="application/javascript" src="app/investigations/investigations.controller.js"></script>
		<script type="application/javascript" src="app/investigations/investigations-item.controller.js"></script>
		<script type="application/javascript" src="app/emails/emails.controller.js"></script>
		<script type="application/javascript" src="app/emails/emails-item.controller.js"></script>
		<script type="application/javascript" src="app/emails/emails-send.controller.js"></script>
		<script type="application/javascript" src="app/calendar/calendar.controller.js"></script>
		<script type="application/javascript" src="app/events/events.controller.js"></script>
		<script type="application/javascript" src="app/events/events-item.controller.js"></script>
		<script type="application/javascript" src="app/users/users.controller.js"></script>
		<script type="application/javascript" src="app/users/users-item.controller.js"></script>
		<script type="application/javascript" src="app/categories/categories.controller.js"></script>
		<script type="application/javascript" src="app/configuration/configuration.controller.js"></script>


		<!-- Libs script -->
		<script src="assets/libs/sweet-alert/sweetalert2.js"></script>
		<script type="application/javascript" src="assets/libs/calendar/moment.js"></script>
		<script type="application/javascript" src="assets/libs/calendar/fullcalendar.js"></script>
		<script type="application/javascript" src="assets/libs/calendar/es.js"></script>
		<script type="application/javascript" src="assets/libs/kendo-calendar/kendo.all.min.js"></script>
		<script type="application/javascript" src="assets/libs/kendo-calendar/kendo.culture.es-ES.min.js"></script>
		<script type="application/javascript" src="assets/libs/jquery-clockpicker/jquery-clockpicker.js"></script>
		<script type="application/javascript" src="assets/libs/angular-switch/angular-ui-switch.js"></script>
		<script type="application/javascript" src="assets/libs/rzslider/rzslider.min.js"></script>
		<script type="application/javascript" src="assets/libs/angular-minicolors/jquery.minicolors.js"></script>
		<script type="application/javascript" src="assets/libs/angular-minicolors/angular-minicolors.js"></script>
		<script type="application/javascript" src="assets/libs/ngtimeago/ngtimeago.js"></script>
		<script type="application/javascript" src="assets/libs/textAngular/textAngular-rangy.min.js"></script>
		<script type="application/javascript" src="assets/libs/textAngular/textAngular-sanitize.min.js"></script>
		<script type="application/javascript" src="assets/libs/textAngular/textAngular.min.js"></script>


		<!-- Custom script -->
		<script type="application/javascript" src="assets/js/index.js"></script>
	</body>
</html>