<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			DASHBOARD
			<div>
			</div>
		</h1>
		<hr>
	</div>



	<div class="content-body">
		<div class="tile-group dashboard">
			<div class="tile-item-content form-primary">

				<div ng-if="!isAdmin()" class="no-admin">
					<img ng-src="assets/images/logo-full.png" class="logo-login"alt="Logotipo"/>
				</div>







				<h1 ng-if="isAdmin()">
					Actividad de usuario
				</h1>
				<div ng-if="isAdmin()" class="log-container">
					<div class="log-content">

						<div ng-if="storage.logactivities.length == 0" class="no-log-items">
							<img ng-src="assets/images/empty.png"/>
							<h2>No existe registro de actividad</h2>
						</div>

						<div ng-if="storage.logactivities.length > 0" ng-repeat="log in storage.logactivities" class="log-item">
							<div class="img-logactivities">
								<img resizeimgresponsive ng-src="{{ log.user_avatar}}"/>
							</div>
							<div class="text-logactivities">
								<div class="link-tooltip" ng-if="isSuperUser()">
									<a target="_blank" ng-href="/EP-Dashboard/usuario/{{ log.id_user}}">
										<!--									<a target="_blank" ng-href="/dashboard/usuario/{{ log.id_user}}">-->
										<i class="" data-title="Ver Perfil" alt="Ver Perfil" >
											<span>{{ log.user_name}}</span>
										</i>
									</a>
								</div>
								<span ng-if="!isSuperUser()">{{ log.user_name}}</span>

								ha

								<span> {{ log.logaction}} </span>

								<div class="link-tooltip">
									<a target="_blank" ng-if="log.logaction != 'eliminado' && log.link" ng-href="/EP-Dashboard/{{log.link}}/{{ log.id_item}}">
										<!--									<a target="_blank" ng-if="log.logaction != 'eliminado' && log.link" ng-href="/dashboard/{{log.link}}/{{ log.id_item}}">-->
										<i data-title="Ver {{log.logsection| deleteArticle}}" alt="Ver {{log.logsection}}">
											<span>{{ log.logsection}}</span>
										</i>
									</a>
									<a ng-if="log.logaction == 'eliminado' || !log.link">
										<i >
											<span>{{ log.logsection}}</span>
										</i>
									</a>
								</div>



								<span ng-if="isSuperUser() && (log.id_user != log.id_user_original)">
									de
								</span>
								<div class="link-tooltip" ng-if="isSuperUser() && (log.id_user != log.id_user_original)">
									<a target="_blank" ng-href="/EP-Dashboard/usuario/{{ log.id_user_original}}">
										<!--									<a target="_blank" ng-href="/dashboard/usuario/{{ log.id_user_original}}">-->
										<i class="" data-title="Ver Perfil" alt="Ver Perfil" >
											<span>{{ log.user_original_name}}</span>
										</i>
									</a>
								</div>
								<span ng-if="!isSuperUser() && (log.id_user != log.id_user_original)">
									de {{ log.user_original_name}}
								</span>

							</div>
							<div class="time-logactivities">
								{{ log.created_at | timeago}}
							</div>
						</div>
					</div>
				</div>





			</div>
		</div>
	</div>
</div>