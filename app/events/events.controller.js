/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("EventsController", function ($rootScope) {
	// If user is not admin, user out
	$rootScope.checkAdmin();

	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'event',
		nameDB: 'events',
		name: 'eventos',
		nameUrl: 'eventos',
		itemName: 'evento'
	};
	$rootScope.filters = {};
	$rootScope.filters.show = false;
	$rootScope.filters.sortOptions = {
		'text': 'Descripción',
		'datetime_start': 'Fecha Inicio',
		'datetime_end': 'Fecha Fin'
	};
	/******************/
	/* END VARIABLES  */
	/******************/



	/*******************/
	/* START ONLOAD */
	/*******************/
	$rootScope.resetFilters();
	$rootScope.readList();
	/*****************/
	/* END ONLOAD */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

