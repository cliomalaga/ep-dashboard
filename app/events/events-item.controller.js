/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("EventsItemController", function ($rootScope, $scope, $stateParams, $timeout) {
	// If user is not admin, user out
	$rootScope.checkAdmin();

	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'event',
		nameDB: 'events',
		name: 'eventos',
		nameUrl: 'eventos',
		itemName: 'evento'
	};
	/******************/
	/* END VARIABLES  */
	/******************/


	/*******************/
	/* START FUNCTION */
	/*******************/
	//Function to create empty item fields
	$scope.newItem = function () {
		$rootScope.item = {};
		$rootScope.item.description = '';
		$rootScope.item.image = '';
		$rootScope.item.datetime_start = '';
		$rootScope.item.time_start = '';
		$rootScope.item.datetime_end = '';
		$rootScope.item.time_end = '';
		$rootScope.item.active = false;
	};
	/*****************/
	/* END FUNCTION */
	/*****************/


	/*******************/
	/* START ONLOAD */
	/*******************/
	// If item ID exist, the item is editing. If not exist ID, the item is new
	if ($stateParams.ID) {
		$rootScope.item.id = $stateParams.ID;
		$rootScope.readItem();
	} else {
		$scope.newItem();
	}


	$timeout(function () {
		$('[clockpicker]').clockpicker({
			'placement': 'top',
			'align': 'left',
			'autoclose': true,
			'default': 'now'
		});
	}, 1000);
	/*****************/
	/* END ONLOAD */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

