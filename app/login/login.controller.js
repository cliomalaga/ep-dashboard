/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("LoginController", function ($rootScope, $scope, $timeout, $location, App) {
	/******************/
	/* VARIABLES      */
	/******************/
//	$scope.email = "gabrielvperez@gmail.com";
//	$scope.password = "secret";
	$rootScope.user = {
		'email': App.decryptedLS('resu'),
		'password': App.decryptedLS('drowssap'),
		'remember': false
	};
	if (App.decryptedLS('rebmemer')) {
		$rootScope.user.remember = true;
	}
	/******************/
	/* END VARIABLES  */
	/******************/

	/*******************/
	/* START FUNCTIONS */
	/*******************/
	// Function to login user
	$scope.login = function () {
		$scope.dataLoading = true;
		App.Login($rootScope.user).then(function (data) {
			if (App.responseOK(data)) {
				App.SetCredentials(data.response.user.id, $scope.email, $scope.password, data.response.token);
//				// Timeout to wait create PHP session
				$timeout(function () {
					if ($rootScope.user.remember) {
						App.encryptedLS('resu', $rootScope.user.email);
						App.encryptedLS('drowssap', $rootScope.user.password);
						App.encryptedLS('rebmemer', 'true');
						App.encryptedLS('dekcol', 'false');
						delete $rootScope.user;
					} else {
						localStorage.clear();
					}
					$location.path('/');
				});
			} else {
				$scope.dataLoading = false;
			}
		});
	};
	/*****************/
	/* END FUNCTIONS */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

