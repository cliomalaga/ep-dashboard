<div class="login-left">
	<img ng-src="assets/images/background-login.jpg" class="img-login" alt="Logotipo"/>
</div>

<div class="login-right">
	<div class="login-content">
		<img ng-src="assets/images/logo-full.png" class="logo-login"/>
		<form ng-submit="login()" name="form" role="form" class="form-primary">
			<div class="form-group form-entry display-inline-block" ng-class="{
				'has-error'
				: form.email.$dirty && form.email.$error.required }">
				<label for="email" class="control-label">Usuario</label>
				<input type="text" name="email" id="email" class="form-control" ng-model="user.email" required autofocus/>
			</div>
			<div class="form-group form-entry display-inline-block" ng-class="{
				'has-error'
				: form.password.$dirty && form.password.$error.required }">
				<label for="password" class="control-label">Contraseña</label>
				<input type="password" name="password" id="password" class="form-control" ng-model="user.password" required />
			</div>
			<div class="form-actions form-entry remember">
				<label class="control-label">Recordar contraseña</label>
				<label for="remember" class="label-checkbox">
					<input type="checkbox" id="remember" name="remember" ng-checked="{{user.remember}}" ng-model="user.remember">
					<div></div>
				</label>
			</div>
			<div class="form-actions form-entry text-right form-submit">
				<button type="submit" ng-disabled="form.$invalid || dataLoading" class="press-button btn btn-item btn-primary-inverse">
					<i ng-if="!dataLoading" class="entypo-login"></i>
					<i ng-if="dataLoading" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
					Login
				</button>
			</div>
		</form>
	</div>
</div>