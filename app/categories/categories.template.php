<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                              ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			<span>
				{{ actualSection.name | uppercase }}
			</span>
			<div>
				<button type="button" ng-click="undo(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-ccw" data-title="Deshacer" alt="Deshacer" ></i>
				</button>
				<button type="button" ng-click="save(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-save" data-title="Guardar" alt="Guardar" ></i>
				</button>
			</div>
		</h1>
		<hr>
	</div>



	<div class="content-body">
		<div class="tile-group">
			<div class="tile-item-content form-primary">
				<div class="form-entry" ng-if="storage.categories">
					<label class="control-label">Categorías</label>
					<select ng-change="selectCategory()" ng-model="item.id_category" ng-options="option.id as option.name for option in storage.categories">
						<option value="">Seleccionar categoría ...</option>
					</select>
				</div>

				<br>

				<div class="form-entry" ng-if="storage.subcategories">
					<label class="control-label">Subcategorías</label>
					<select ng-change="selectSubcategory()" ng-model="item.id" ng-options="option.id as option.name for option in filteredItems = (storage.subcategories| onlyCategory: item.id_category)">
						<option value="">Seleccionar subcategoría ...</option>
					</select>
				</div>

				<br>

				<div class="form-entry form-delete-input">
					<label class="control-label" ng-if="!item.id">Añadir Subcategoría</label>
					<label class="control-label" ng-if="item.id">Editar Subcategoría</label>
					<input type="text" class="form-control" name="name" ng-model="item.name">
					<i ng-click="deleteCategory(item)" ng-if="item.id" class="entypo-cancel delete-input" data-title="Eliminar Categoría" alt="Eliminar Categoría" ></i>
				</div>
			</div>
		</div>
	</div>
</div>