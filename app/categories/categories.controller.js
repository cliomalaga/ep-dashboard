/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("CategoriesController", function ($rootScope, $scope) {
	//	If user is not superuser, user out
	$rootScope.checkSuperUser();

	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'category',
		nameDB: 'categories',
		name: 'categorías',
		nameUrl: 'categorias',
		itemName: 'categorias'
	};
	$rootScope.item = {};
	$rootScope.itemOriginal = angular.copy($rootScope.item);
	/******************/
	/* END VARIABLES  */
	/******************/


	/*******************/
	/* START ONLOAD */
	/*******************/
	$rootScope.readList();
	/*****************/
	/* END ONLOAD */
	/*****************/


	/*******************/
	/* START FUNCTIONS */
	/*******************/
	// Function call to select Category
	$scope.selectCategory = function () {
		$rootScope.itemOriginal = angular.copy($rootScope.item);
	};


	// Function call to select Subcategory
	$scope.selectSubcategory = function () {
		var i = $rootScope.findInArray($rootScope.item, $rootScope.storage.subcategories);
		if (i != undefined) {
			$rootScope.item.name = $rootScope.storage.subcategories[i].name;
		} else {
			$rootScope.item.name = '';
		}
		$rootScope.itemOriginal = angular.copy($rootScope.item);
	};


	// Function to update changes if item is editing
	$scope.save = function (_item) {
		$rootScope.actualSection.nameDB = 'subcategories';
		var redirect = false;
		$scope.$parent.save(_item, redirect);
	};


	// Function to delete item
	$scope.deleteCategory = function (_item) {
		$rootScope.actualSection.nameDB = 'subcategories';
		$scope.$parent.delete(_item);
	};
	/*****************/
	/* END FUNCTIONS */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

