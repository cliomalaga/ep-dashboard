<div class="content-body table">
	<div class="row">
		<div class="no-results">
			<img ng-if="collection.length == 0" ng-src="assets/images/empty.png"/>
			<h2 ng-if="collection.length == 0">No existen {{actualSection.name}}</h2>
			<img ng-if="collection.length > 0" ng-hide="filteredItems.length" ng-src="assets/images/no-found.png"/>
			<h2 ng-if="collection.length > 0" ng-hide="filteredItems.length">No hay coincidencias</h2>
		</div>


		<div class="table table-bordered responsive col-xs-12"
			 ng-hide="collection.length == 0 || filteredItems.length == 0">
			<div class="thead">
				<div class="tr">
					<div ng-if="isExistProperty(collection[0], 'name')" class="th">Nombre</div>
					<div ng-if="isExistProperty(collection[0], 'lastname')" class="th">Apellidos</div>
					<div ng-if="isExistProperty(collection[0], 'email')" class="th">Email</div>
					<div ng-if="isExistProperty(collection[0], 'about')" class="th">Sobre mi</div>
					<div ng-if="isExistProperty(collection[0], 'title')" class="th">Título</div>
					<div ng-if="isExistProperty(collection[0], 'subtitle')" class="th">Subtítulo</div>
					<div ng-if="isExistProperty(collection[0], 'text')" class="th">Descripción</div>
					<div ng-if="isExistProperty(collection[0], 'description')" class="th">Descripción</div>
					<div ng-if="isExistProperty(collection[0], 'author')" class="th">Autor</div>
					<div ng-if="isExistProperty(collection[0], 'datetime_start')" class="th">Fecha Inicio</div>
					<div ng-if="isExistProperty(collection[0], 'datetime_end')" class="th">Fecha Fin</div>
					<div ng-if="isExistProperty(collection[0], 'attached')" class="th">Adjunto</div>
					<div class="th"></div>
				</div>
			</div>
			<div class="tbody">
				<div ng-repeat="item in filteredItems = (collection| filter : searchItem | sortItemBy : filters.sortField : filters.sortOrder | emptyToEnd: filters.sortField)"
					 ng-class="{'item-active':isActive(item)}" class="tr">
					<!--UESRS-->
					<div ng-if="isExistProperty(item, 'name')" class="td td-title">
						<div>{{ item.name}}</div>
					</div>
					<div ng-if="isExistProperty(item, 'lastname')" class="td">
						<div>{{ item.lastname}}</div>
					</div>
					<div ng-if="isExistProperty(item, 'email')" class="td">
						<div>{{ item.email}}</div>
					</div>
					<div ng-if="isExistProperty(item, 'about')" class="td">
						<div>{{ item.about}}</div>
					</div>
					<!--ITEMS-->
					<div ng-if="isExistProperty(item, 'title')" class="td td-title">
						<div>{{ item.title}}</div>
					</div>
					<div ng-if="isExistProperty(item, 'subtitle')" class="td td-title">
						<div>{{ item.subtitle}}</div>
					</div>
					<div ng-if="isExistProperty(item, 'text_pretty')" class="td td-title">
						<div>{{ item.text_pretty}}</div>
					</div>
					<div ng-if="isExistProperty(item, 'author')" class="td td-title">
						<div>{{ item.author}}</div>
					</div>

					<!--EVENTS-->
					<div ng-if="isExistProperty(item, 'description_pretty')" class="td">
						<div>
							<p ng-text-truncate="item.description_pretty"
							   ng-tt-chars-threshold="100" ng-tt-no-toggling>
								{{ item.description_pretty}}
							</p>
						</div>
					</div>
					<div ng-if="isExistProperty(item, 'datetime_start')" class="td">
						<div>{{ item.datetime_start}}</div>
					</div>
					<div ng-if="isExistProperty(item, 'datetime_end')" class="td">
						<div>{{ item.datetime_end}}</div>
					</div>

					<!--ATTACHEDS-->
					<div ng-if="isExistProperty(item, 'attached')" class="td td-title">
						<div>
							<a ng-if="item.attached" href="{{item.attached}}" download target="_self" data-title="Descargar adjunto" alt="Descargar adjunto">
								{{ item.attached | onlyAttachedName}}
							</a>
						</div>
					</div>


					<div ng-if="isOwn(item)" class="td">

						<div>
							<a class="button-view press-button">
								<i class="entypo-pencil" ng-click="edit(item)" data-title="Editar" alt="Editar"></i>
							</a>
							<a ng-if="isActive(item)" class="button-desactivate press-button">
								<i class="entypo-block" ng-click="activateDesactivate(item)" data-title="Desactivar" alt="Desactivar"></i>
							</a>
							<a ng-if="!isActive(item)" class="button-desactivate press-button">
								<i class="entypo-block" ng-click="activateDesactivate(item)" data-title="Activar" alt="Activar"></i>
							</a>
							<a class="button-delete press-button">
								<i class="entypo-cancel" ng-click="delete(item)" data-title="Eliminar" alt="Eliminar"></i>
							</a>
						</div>
						<div ng-if="!isOwn(item)" class="td">
							<div>
								<a class="button-view press-button">
									<i class="entypo-publish" ng-click="edit(item)" data-title="Ver Detalles" alt="Ver Detalles"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>