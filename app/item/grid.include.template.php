<div class="content-body grid">
	<div class="row">
		<div class="no-results">
			<img ng-if="collection.length == 0" ng-src="assets/images/empty.png"/>
			<h2 ng-if="collection.length == 0">No existen {{actualSection.name}}</h2>
			<img ng-if="collection.length > 0" ng-hide="filteredItems.length" ng-src="assets/images/no-found.png"/>
			<h2 ng-if="collection.length > 0" ng-hide="filteredItems.length">No hay coincidencias</h2>
		</div>

		<div ng-if="!item.delete_user || item.delete_user == false" ng-repeat="item in filteredItems = (collection| filter : searchItem | sortItemBy : filters.sortField : filters.sortOrder | emptyToEnd: filters.sortField)"
			 class="col-sm-6 col-lg-4 item">
			<div class="tile-progress tile-primary"
				 ng-class="{'active' : isActive(item), 'desactive' : !isActive(item)}">
				<div class="tile-header" ng-class="{'empty': !item.image && !item.avatar}">
					<img ng-if="item.image" class="image-item" ng-src="{{ item.image}}" ng-class="{'display-block' :  !isPreview(item)}"/>
					<img ng-if="item.avatar" class="image-item" ng-src="{{ item.avatar}}" ng-class="{'display-block' :  !isPreview(item)}"/>
				</div>


				<div class="tile-footer">
					<!--USERS-->
					<h3 class="name" ng-if="item.name">{{ item.name}}</h3>
					<span class="description" ng-if="item.lastname">{{ item.lastname}}</span>
					<span class="description" ng-if="item.email">{{ item.email}}</span>
					<span class="description" ng-if="item.about">{{ item.about}}</span>
					<!--ITEMS-->
					<h3 class="name" ng-if="item.title">{{ item.title}}</h3>
					<span class="description" ng-if="item.subtitle">{{ item.subtitle}}</span>
					<span class="description" ng-if="item.author">{{ item.author}}</span>
					<span class="description" ng-if="item.attached">
						<a href="{{item.attached}}" download target="_self" data-title="Descargar adjunto" alt="Descargar adjunto">
							{{ item.attached | onlyAttachedName}}
						</a>
					</span>
					<!--EVENTS-->
					<span class="description" ng-if="item.description_pretty">{{ item.description_pretty}}</span>
					<span class="date" ng-if="item.datetime_start">Desde {{ item.datetime_start}}</span>
					<span class="date" ng-if="item.datetime_end">Hasta {{ item.datetime_end}}</span>


					<div class="tile-footer-buttons" ng-if="isOwn(item)">
						<a class="button-view press-button">
							<i class="entypo-pencil" ng-click="edit(item)" data-title="Editar" alt="Editar"></i>
						</a>
						<a ng-if="isActive(item)" class="button-desactivate press-button">
							<i class="entypo-block" ng-click="activateDesactivate(item)" data-title="Desactivar" alt="Desactivar"></i>
						</a>
						<a ng-if="!isActive(item)" class="button-desactivate press-button">
							<i class="entypo-block" ng-click="activateDesactivate(item)" data-title="Activar" alt="Activar"></i>
						</a>
						<a class="button-delete press-button">
							<i class="entypo-cancel" ng-click="delete(item)" data-title="Eliminar" alt="Eliminar"></i>
						</a>
					</div>
					<div class="tile-footer-buttons" ng-if="!isOwn(item)">
						<a class="button-view press-button">
							<i class="entypo-publish" ng-click="edit(item)" data-title="Ver Detalles" alt="Ver Detalles"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>