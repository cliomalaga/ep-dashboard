<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                  ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			<span>
				{{ actualSection.name | uppercase }}
			</span>
			<div>
				<button type="button" ng-if="filters.show" ng-click="resetFilters()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-ccw"  data-title="Restaurar Filtros" alt="Restaurar Filtros" ></i>
				</button>
				<button type="button" ng-if="filters.show" ng-click="sortInverse()" class="press-button btn btn-item btn-primary-inverse">
					<i ng-if="filters.sortOrder" class="icon-arrow-up"  data-title="Orden Ascendente" alt="Orden Ascendente" ></i>
					<i ng-if="!filters.sortOrder" class="icon-arrow-down"  data-title="Orden Descendente" alt="Orden Descendente" ></i>
				</button>
				<span ng-if="filters.show">|</span>
				<button type="button" ng-click="create()" class="press-button btn btn-item btn-primary-inverse">
					<i class="entypo-plus" data-title="Añadir {{actualSection.itemName}}" alt="Añadir {{actualSection.itemName}}" ></i>
				</button>
				<button type="button" ng-click="toogleFilters()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-funnel" ng-if="!filters.show" data-title="Mostrar Filtros" alt="Mostrar Filtros" ></i>
					<i class="icon-funnel" ng-if="filters.show" data-title="Ocultar Filtros" alt="Ocultar Filtros" ></i>
				</button>
				<button type="button" ng-click="toogleViews()" class="press-button btn btn-item btn-primary-inverse">
					<i class="entypo-eye" data-title="Cambiar vista" alt="Cambiar vista" ></i>
				</button>
			</div>
		</h1>
		<hr>
	</div>
	<div class="content-filter" ng-if="filters.show">
		<div>
			<div class="tile-item-content form-primary" >
				<div class="form-entry">
					<input type="text" focus-me="filters.focus" class="form-control" name="name" ng-model="filters.search" placeholder="Buscar...">
				</div>
				<div class="form-entry">
					<select ng-model="filters.sortField" ng-options="k as v for (k, v) in filters.sortOptions">
						<option value="">Ordenar por...</option>
					</select>
				</div>
			</div>
		</div>
		<hr>
	</div>

	<div ng-if="configuration.view == 'grid'" ng-include="'app/item/grid.include.template.php'" class="container-include"></div>
	<div ng-if="configuration.view == 'list'" ng-include="'app/item/list.include.template.php'" class="container-include"></div>
	<div ng-if="configuration.view == 'table'" ng-include="'app/item/table.include.template.php'" class="container-include"></div>
</div>