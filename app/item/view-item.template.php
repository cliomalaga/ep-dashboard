<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			<span>
				{{ actualSection.name | uppercase }}
			</span>
			<div>
				<button type="button" ng-click="back(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-chevron-left" data-title="Volver" alt="Volver" ></i>
				</button>
				<button  ng-if="isOwn(item) && item.id" type="button" ng-click="undo(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-ccw" data-title="Deshacer" alt="Deshacer" ></i>
				</button>
				<button  ng-if="isOwn(item) && !item.id" type="button" ng-click="newItem()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-ccw" data-title="Deshacer" alt="Deshacer" ></i>
				</button>
				<button ng-disabled="isExistProperty(item, 'id_subcategory') && !item.id_subcategory" ng-if="isOwn(item)" type="button" ng-click="save(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-save" data-title="Guardar" alt="Guardar" ></i>
				</button>
			</div>
		</h1>
		<hr>
	</div>



	<div class="content-body">

		<div class="tile-group">

			<div class="tile-image" ng-class="{'empty': !item.image && !item.images.url}">
				<img ng-if="item.image" class="image-item" ng-src="{{ item.image}}" ng-class="{'display-block' :  !isPreview(item)}"/>

				<img class="image-item preview" ng-src="{{ item.images.url}}" ng-class="{'display-block' :  isPreview(item)}"/>

				<p ng-if="isOwn(item)">
					<label for="image">
						<input type="file" id="image" name="image" ng-file-model="item.images" ng-model="item.images" class="inputfile" />
						<i data-title="Cambiar imagen" alt="Cambiar imagen"></i>
					</label>
					<label ng-click="deleteImage(item)" ng-if="item.image || item.images">
						<i data-title="Eliminar imagen" alt="Eliminar imagen"></i>
					</label>
				</p>
			</div>

			<div class="tile-item-content form-primary">
				<div class="form-entry form-createdby" ng-if="item.id && isExistProperty(item, 'created_by')">
					<label class="control-label">
						<i class="fa fa-fw fa-user"></i>&nbsp;
						Creado por
					</label>
					{{ item.created_by.name}} {{ item.created_by.lastname}}
				</div>
				<!--ITEM-->
				<div class="form-entry" ng-if="storage.subcategories && isExistProperty(item, 'id_subcategory')">
					<label class="control-label">
						<i class="fa fa-fw fa-tag"></i>&nbsp;
						Subcategoría
					</label>
					<select id="select" ng-model="item.id_subcategory" ng-options="option.id as option.name for option in filteredItems = (storage.subcategories| onlyCategory: actualSection.name)">
						<option value="">Seleccionar subcategoría ...</option>
					</select>
				</div>
				<div class="form-entry" ng-if="isExistProperty(item, 'title')">
					<label class="control-label">
						<i class="icon icon-browser"></i>&nbsp;
						Título
					</label>
					<input type="text" class="form-control" name="title" ng-model="item.title" ng-disabled="!isOwn(item)">
				</div>
				<div class="form-entry" ng-if="isExistProperty(item, 'subtitle')">
					<label class="control-label">
						<i class="icon icon-level-down"></i>&nbsp;
						Subtítulo
					</label>
					<input type="text" class="form-control" name="subtitle" ng-model="item.subtitle" ng-disabled="!isOwn(item)">
				</div>
				<div class="form-entry" ng-if="isExistProperty(item, 'author')">
					<label class="control-label">
						<i class="fa fa-fw fa-user"></i>&nbsp;
						Autor de la respuesta
					</label>
					<input type="text" class="form-control" name="author" ng-model="item.author" ng-disabled="!isOwn(item)">
				</div>
				<div class="form-entry" ng-if="isExistProperty(item, 'text')">
					<label class="control-label">
						<i class="icon icon-quote"></i>&nbsp;
						Descripción
					</label>
					<div text-angular ng-model="item.text"></div>
				</div>
				<!--EVENTS-->
				<div class="form-entry" ng-if="isExistProperty(item, 'description')">
					<label class="control-label">
						<i class="icon icon-quote"></i>&nbsp;
						Descripción
					</label>
					<div text-angular ng-model="item.description"></div>
				</div>
				<div class="form-entry" ng-if="isExistProperty(item, 'datetime_start')">
                    <label class="control-label">
						<i class="fa fa-fw fa-calendar-o"></i>&nbsp;
						Fecha Inicio
					</label>
                    <input kendo-date-picker ng-model="item.datetime_start"  k-options="calendarOptions" ng-disabled="!isOwn(item)"/>
                </div>
                <div class="form-entry" ng-if="isExistProperty(item, 'datetime_start')">
                    <label class="control-label">
						<i class="fa fa-fw fa-clock-o"></i>&nbsp;
						Hora Inicio
					</label>
                    <div clockpicker class="input-group clockpicker">
                        <input id="start" type="text" class="form-control" ng-model="item.time_start" ng-disabled="!isOwn(item)">
                    </div>
                </div>
                <div class="form-entry" ng-if="isExistProperty(item, 'datetime_end')">
                    <label class="control-label">
						<i class="fa fa-fw fa-calendar-o"></i>&nbsp;
						Fecha Final
					</label>
                    <input kendo-date-picker ng-model="item.datetime_end"  k-options="calendarOptions" ng-disabled="!isOwn(item)"/>
                </div>
                <div class="form-entry" ng-if="isExistProperty(item, 'datetime_end')">
                    <label class="control-label">
						<i class="fa fa-fw fa-clock-o"></i>&nbsp;
						Hora Final
					</label>
                    <div clockpicker class="input-group clockpicker">
                        <input id="end" type="text" class="form-control" ng-model="item.time_end" ng-disabled="!isOwn(item)">
                    </div>
                </div>
				<!--ATTACHEDS-->
				<div class="form-entry form-delete-input" ng-if="isExistProperty(item, 'attached')">
					<label class="control-label">
						<i class="icon icon-attachment"></i>&nbsp;
						Adjunto
					</label>
					<label for="attached" class="label-input-file">
						<span ng-if="!item.attacheds.name">{{ item.attached | onlyAttachedName}}</span>
						<span ng-if="item.attacheds.name">{{ item.attacheds.name}}</span>
					</label>
					<input type="file" class="form-control" id="attached" name="attached" ng-file-model="item.attacheds" ng-model="item.attacheds" ng-disabled="!isOwn(item)">
					<i ng-click="deleteAttached(item)" ng-if="item.id && item.attached" class="entypo-cancel delete-input" data-title="Eliminar Adjunto" alt="Eliminar Adjunto" ></i>
					<a ng-if="item.id && item.attached" href="{{item.attached}}" download target="_self" data-title="Descargar adjunto" alt="Descargar adjunto" class="download-input" >
						<i class="entypo-download"></i>
					</a>
				</div>


				<div class="form-actions form-entry remember" ng-if="isExistProperty(item, 'active')">
					<label class="control-label">
						<i class="fa fa-fw fa-check"></i>&nbsp;
						Activo
					</label>
					<label for="active" class="label-checkbox">
						<input type="checkbox" forceboolean id="active" name="active" ng-checked="isActive(item)"
							   ng-model="item.active" ng-disabled="!isOwn(item)">
						<div></div>
					</label>
				</div>
			</div>






		</div>
	</div>
</div>