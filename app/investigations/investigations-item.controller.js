/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("InvestigationsItemController", function ($rootScope, $scope, $stateParams) {
	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'investigation',
		nameDB: 'investigations',
		name: 'investigaciones',
		nameUrl: 'investigaciones',
		itemName: 'investigacion'
	};
	/******************/
	/* END VARIABLES  */
	/******************/


	/*******************/
	/* START FUNCTION */
	/*******************/
	//Function to create empty item fields
	$scope.newItem = function () {
		$rootScope.item = {};
		$rootScope.item.id_subcategory = null;
		$rootScope.item.title = '';
		$rootScope.item.subtitle = '';
		$rootScope.item.text = '';
		$rootScope.item.image = '';
		$rootScope.item.attached = '';
		$rootScope.item.active = false;
	};
	/*****************/
	/* END FUNCTION */
	/*****************/


	/*******************/
	/* START ONLOAD */
	/*******************/
	// If item ID exist, the item is editing. If not exist ID, the item is new
	if ($stateParams.ID) {
		$rootScope.item.id = $stateParams.ID;
		$rootScope.readItem();
	} else {
		$scope.newItem();
	}
	/*****************/
	/* END ONLOAD */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

