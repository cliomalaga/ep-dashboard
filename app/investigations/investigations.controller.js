/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("InvestigationsController", function ($rootScope) {
	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'investigation',
		nameDB: 'investigations',
		name: 'investigaciones',
		nameUrl: 'investigaciones',
		itemName: 'investigacion'
	};

	$rootScope.filters = {};
	$rootScope.filters.show = false;
	$rootScope.filters.sortOptions = {
		'title': 'Título',
		'subtitle': 'Subtítulo',
		'text': 'Descripción',
	};
	/******************/
	/* END VARIABLES  */
	/******************/



	/*******************/
	/* START ONLOAD */
	/*******************/
	$rootScope.resetFilters();
	$rootScope.readList();
	/*****************/
	/* END ONLOAD */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

