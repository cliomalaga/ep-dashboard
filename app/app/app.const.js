app.constant("URL_ROOT", "/EP-Dashboard/");
//app.constant("URL_ROOT", "/dashboard/");
app.constant("URL_PHP", "php/");
app.constant("DELAY_NOTIFICATION", "5"); // Seconds
app.constant("DELAY_IDLE", "30"); // Minuts
app.constant("KEY_LS", document.domain.replace(".", ""));
app.constant("DATE_DEFAULT", "1970-01-01T00:00");