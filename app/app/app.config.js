app.run(['$rootScope', '$cookieStore', '$http', '$location', '$idle', '$state', 'URL_ROOT', function ($rootScope, $cookieStore, $http, $location, $idle, $state, URL_ROOT) {
		// Fix link button in ui-sref attribute
		$state.originalHrefFunction = $state.href;
		$state.href = function href(stateOrName, params, options) {
			var result = $state.originalHrefFunction(stateOrName, params, options);
			if (result && result.slice(0, 1) === '/') {
				return URL_ROOT + result.replace('/', '');
			} else {
				return result;
			}
		}
		// Keep user logged in after page refresh
		$rootScope.forceLogin = function () {
			$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
			if ($location.path() == '/login') {
				$rootScope.selectSection('dashboard');
				$location.path('/');
			}
		};

		$rootScope.globals = $cookieStore.get('authenticate') || {};
		if ($rootScope.globals.currentUser) { // If user logued exist, force log in app
			$rootScope.forceLogin();
		}

		/*****************************************/
		/* CHANGE STATE START (NAVIGATION VIEWS) */
		/*****************************************/
		$rootScope.$on('$stateChangeStart', function () {
			$rootScope.isMenuResponsiveOpen = false;
			// Redirect to login page if not logged in and trying to access a restricted page
			var restrictedPage = $.inArray($location.url, ['/login']) === -1;
			var loggedIn = $rootScope.globals.currentUser;
			if (restrictedPage && !loggedIn) {
				$location.path('/login');
			}
		});
		/*********************************************/
		/* END CHANGE STATE START (NAVIGATION VIEWS) */
		/*********************************************/


		/*********************************************/
		/* START RUN */
		/*********************************************/
		$idle.watch();
		/*********************************************/
		/* END RUN */
		/*********************************************/
	}]);




app.config(function AppConfig($provide, $urlRouterProvider, $stateProvider, $locationProvider, $idleProvider, $keepaliveProvider, toasterConfig, DELAY_NOTIFICATION, DELAY_IDLE) {
	// IDLE CONFIGUURATION
	$idleProvider.idleDuration(DELAY_IDLE * 60); // in minuts
//	$idleProvider.idleDuration(DELAY_IDLE); // in seconds
	$idleProvider.warningDuration(10); // in seconds
	$keepaliveProvider.interval(2); // in seconds


	// NOTIFICATION
	angular.extend(toasterConfig, {
		'limit': 0,
		'tap-to-dismiss': true,
		'close-button': false,
		'newest-on-top': true,
		'time-out': DELAY_NOTIFICATION,
		'position-class': 'toast-top-right',
		'icon-classes': {
			success: 'entypo-thumbs-up',
			error: 'entypo-thumbs-down'
		}
	});


	// TEXT RICH
	$provide.decorator('taOptions', ['$delegate', function (taOptions) {
			taOptions.toolbar = [
				['h1', 'p', 'bold', 'italics', 'underline', 'ul', 'redo', 'undo', 'clear', 'insertLink']
			];
			return taOptions;
		}]);


	// ROUTES
	$urlRouterProvider.otherwise('/');
	$locationProvider.html5Mode(true);
	$locationProvider.hashPrefix('!');
	$stateProvider
		// Views
		.state('login', {
			url: '/login',
			views: {
				'app-view': {
					templateUrl: 'app/login/login.template.php',
					controller: 'LoginController'
				}
			}
		})
		// View abstract to nested views
		.state('app', {
			views: {
				'app-view': {
					templateUrl: 'app/app/app-view.template.php',
					controller: 'AppController'
				}
			}
		})
		.state('template', {
			parent: 'app',
			views: {
				'menu-view': {
					templateUrl: 'app/app/app-menu.template.php'
				},
				'content-view': {
					templateUrl: 'app/app/app-content.template.php'
				}
			}
		})

		// Nested views
		.state('dashboard', {
			url: '/',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/dashboard/dashboard.template.php',
					controller: 'DashboardController'
				}
			}
		})
		.state('perfil', {
			url: '/perfil',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/profile/profile.template.php',
					controller: 'ProfileController'
				}
			}
		})
		.state('pruebas', {
			url: '/pruebas',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-all.template.php',
					controller: 'TestsController'
				}
			}
		})
		.state('prueba', {
			url: "/prueba/:ID",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-item.template.php',
					controller: 'TestsItemController'
				}
			}
		})
		.state('preguntas', {
			url: '/preguntas',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-all.template.php',
					controller: 'FaqsController'
				}
			}
		})
		.state('pregunta', {
			url: "/pregunta/:ID",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-item.template.php',
					controller: 'FaqsItemController'
				}
			}
		})
		.state('documentos', {
			url: '/documentos',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-all.template.php',
					controller: 'DocumentsController'
				}
			}
		})
		.state('documento', {
			url: "/documento/:ID",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-item.template.php',
					controller: 'DocumentsItemController'
				}
			}
		})
		.state('ensenanzas', {
			url: '/ensenanzas',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-all.template.php',
					controller: 'TeachingsController'
				}
			}
		})
		.state('ensenanza', {
			url: "/ensenanza/:ID",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-item.template.php',
					controller: 'TeachingsItemController'
				}
			}
		})
		.state('casos', {
			url: '/casos',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-all.template.php',
					controller: 'CasesController'
				}
			}
		})
		.state('caso', {
			url: "/caso/:ID",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-item.template.php',
					controller: 'CasesItemController'
				}
			}
		})
		.state('investigaciones', {
			url: '/investigaciones',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-all.template.php',
					controller: 'InvestigationsController'
				}
			}
		})
		.state('investigacion', {
			url: "/investigacion/:ID",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-item.template.php',
					controller: 'InvestigationsItemController'
				}
			}
		})
		.state('eventos', {
			url: '/eventos',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-all.template.php',
					controller: 'EventsController'
				}
			}
		})
		.state('evento', {
			url: "/evento/:ID",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-item.template.php',
					controller: 'EventsItemController'
				}
			}
		})
		.state('calendario', {
			url: '/calendario',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/calendar/calendar.template.php',
					controller: 'CalendarController'
				}
			}
		})
		.state('mensajes', {
			url: "/mensajes",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/emails/emails.template.php',
					controller: 'EmailsController'
				}
			}
		})
		.state('mensaje', {
			url: "/mensaje/:ID",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/emails/emails-item.template.php',
					controller: 'EmailsItemController'
				}
			}
		})
		.state('mensajes/enviar', {
			url: "/mensajes/enviar",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/emails/emails-send.template.php',
					controller: 'EmailsSendController'
				}
			}
		})
		.state('usuarios', {
			url: '/usuarios',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/item/view-all.template.php',
					controller: 'UsersController'
				}
			}
		})
		.state('usuario/:ID', {
			url: "/usuario/:ID",
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/users/users-item.template.php',
					controller: 'UsersItemController'
				}
			}
		})
		.state('categorias', {
			url: '/categorias',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/categories/categories.template.php',
					controller: 'CategoriesController'
				}
			}
		})
		.state('configuracion', {
			url: '/configuracion',
			parent: 'template',
			views: {
				'content': {
					templateUrl: 'app/configuration/configuration.template.php',
					controller: 'ConfigurationController'
				}
			}
		})
		.state('configuracion-fake', {
			url: "/configuracion/:ID",
			parent: 'template',
			views: {
				'content': {
					controller: function ($state) {
						$state.go('configuracion');
					}
				}
			}
		})
});

