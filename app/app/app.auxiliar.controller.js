/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* APP CONTROLLER ************************************************************/
app.controller("AuxiliarController", function ($rootScope, $scope, $location, App, DATE_DEFAULT) {
	/******************/
	/* VARIABLES      */
	/******************/
	/******************/
	/* END VARIABLES  */
	/******************/

	/*******************/
	/* FILTERS FUNCTIONS */
	/*******************/
	// Format Date
	$rootScope.formatDate = function (date) {
		return new Date(date);
	};
	// Toogle items views
	$rootScope.toogleViews = function () {
		$scope.n = $rootScope.configuration.views.indexOf($rootScope.configuration.view);
		if ($scope.n !== -1) {
			if ($scope.n == $rootScope.configuration.views.length - 1) {
				$scope.n = 0;
			} else {
				$scope.n++;
			}
			$rootScope.configuration.view = $rootScope.configuration.views[$scope.n];
		}
	};
	// Toogle items views calendar
	$scope.toogleCalendar = function () {
		$scope.n = $rootScope.configuration.viewsCalendar.indexOf($rootScope.configuration.view_calendar);
		if ($scope.n !== -1) {
			if ($scope.n == $rootScope.configuration.viewsCalendar.length - 1) {
				$scope.n = 0;
			} else {
				$scope.n++;
			}
			$rootScope.configuration.view_calendar = $rootScope.configuration.viewsCalendar[$scope.n];
			$('#calendar').fullCalendar('changeView', $rootScope.configuration.view_calendar);
		}
	};
	// Toogle div filters
	$rootScope.toogleFilters = function () {
		$rootScope.resetFilters();
		$rootScope.filters.show = !$rootScope.filters.show;
		$rootScope.filters.focus = true;
	};
	// Init and reset filters
	$rootScope.resetFilters = function () {
		$rootScope.filters.sortField = null;
		$rootScope.filters.search = '';
		$rootScope.filters.sortOrder = false;
	};
	// Sort inverse
	$rootScope.sortInverse = function () {
		$rootScope.filters.sortOrder = !$rootScope.filters.sortOrder;
	};
	// Filter by string
	$rootScope.searchItem = function (item) {
		var search = $rootScope.filters.search;
		var searchLower = search.toLowerCase();
		if ((search.length == 0) ||
			(search && (
				(item.name && (item.name.toLowerCase().indexOf(searchLower) != -1)) ||
				(item.lastname && (item.lastname.toLowerCase().indexOf(search) != -1)) ||
				(item.about && (item.about.toLowerCase().indexOf(search) != -1)) ||
				(item.description && (item.description.toLowerCase().indexOf(search) != -1)) ||
				(item.link && (item.link.toLowerCase().indexOf(search) != -1)) ||
				(item.title && (item.title.toLowerCase().indexOf(search) != -1)) ||
				(item.date && (item.date.indexOf(search) != -1)) ||
				(item.to && (item.to.indexOf(search) != -1)) ||
				(item.subject && (item.subject.indexOf(search) != -1)) ||
				(item.messagge && (item.messagge.indexOf(search) != -1))
				))
			)
		{
			return true;
		}
		return false;
	};
	/*********************/
	/* FILTERS FUNCTIONS */
	/*********************/



	/*******************/
	/* AUXILIAR FUNCTIONS */
	/*******************/
	// Function to redirect
	$rootScope.redirect = function (_url) {
		$location.path(_url);
	};
	//  Function to find item in array
	$rootScope.findInArray = function (_item, _array) {
		for (var i = 0, limit = _array.length; i < limit; i++) {
			if (_array[i].id == _item.id) {
				return i;
			}
		}
	};
	//  Function to find item in array by user
	$rootScope.findInArrayByUser = function (_item, _array) {
		for (var i = 0, limit = _array.length; i < limit; i++) {
			if (_array[i].id_user == _item.id_user) {
				return i;
			}
		}
	};
	//  Function to add class "active" in topbar buttons
	$rootScope.isActiveSection = function (_section) {
		// If is an array, check each item
		if (_section.constructor === Array) {
			for (var i = 0; i < _section.length; i++) {
				if (_section[i] == $rootScope.selected) {
					return true;
				}
			}
		} else {
			// If is an string, check directly
			if (_section == $rootScope.selected) {
				return true;
			}
		}
		return false;
	};
	//  Function to save selected section
	$rootScope.selectSection = function (_section, _hasSubmenu) {
		if (_hasSubmenu == 'undefined') {
			_hasSubmenu == false;
		}
		// If we want close same menu than section selected
		if ((_section == 'menu-portafolio' && ($rootScope.selected == 'trabajos' || $rootScope.selected == 'webs' || $rootScope.selected == 'proyectos' || $rootScope.selected == 'cursos' || $rootScope.selected == 'habilidades'))
			|| (_section == 'menu-mensajes' && ($rootScope.selected == 'mensajes' || $rootScope.selected == 'mensajes/enviar' || $rootScope.selected == 'mensajes'))) {
			$rootScope.selected = '';
		} else {

			// If has submenu or if the menu is responsive, we can close the menu if click on same section
			if (_section == $rootScope.selected && ($rootScope.isMenuResponsiveOpen == true || _hasSubmenu == true)) // Close submenu
				$rootScope.selected = '';
			else
				$rootScope.selected = _section;
		}
	};
	//  Function to add class "active" in any element
	$rootScope.isActive = function (_item) {
		if (_item && parseInt(_item.active) == 1)
			return true;
		else
			return false;
	};
	//  Function to add class "preview" in any element
	$rootScope.isPreview = function (_item) {
		if (_item && (_item.files || _item.images))
			return true;
		else
			return false;
	};
	//  Function to check if exist this property
	$rootScope.isExistProperty = function (_item, _property) {
		if (_item && _item.hasOwnProperty(_property))
			return true;
		else
			return false;
	};
	//  Function to check if item own
	$rootScope.isOwn = function (_item) {
		if ($rootScope.globals && $rootScope.globals.currentUser) {
			if (!_item.id_user || $rootScope.globals.currentUser.id == _item.id_user || $rootScope.isSuperUser()) {
				return true;
			}
		}
		return false;
	};
	// Function to show alert confirm
	$rootScope.modalConfirm = function (_text, _textButton) {
		App.modalConfirm(_text, _textButton);
	};
	// Function to transform boolean to integer
	$rootScope.booleanToInt = function (_boolean) {
		if (_boolean)
			return 1;
		else
			return 0;
	};
	// Function to transform integer to boolean
	$rootScope.intToBoolean = function (_int) {
		_int = parseInt(_int);
		if (_int == 1)
			return true;
		else
			return false;
	};
	// Delete image item
	$scope.deleteImage = function (_item) {
		if (_item.image) {
			_item.image = '';
		}
		if (_item.avatar) {
			_item.avatar = '';
		}
		_item.files = null;
	};
	// Delete attached
	$scope.deleteAttached = function (_item) {
		_item.attached = '';
		_item.attacheds = null;
	};
	// Function to calculate the difference between two dates form DB
	$rootScope.calcDifference = function (_firstDate, _secondDate) {
		if (_firstDate) {
			var date;
			// Parse date to calculate the difference
			if (_secondDate) {
				date = _secondDate.split('/');
				_secondDate = new Date(date[1] + '/' + date[0] + '/' + date[2]);
				_secondDate = new Date(_secondDate);
			} else {
				// If not exist second date, we take now date to calculate the difference
				_secondDate = new Date();
			}
			date = _firstDate.split('/');
			_firstDate = new Date(date[1] + '/' + date[0] + '/' + date[2]);
			if (_secondDate < _firstDate) {
				// If second date is before that first date, we don't calculate the difference
				return false;
			}
			// Parse date to transform in String format date
			var str = "";
			var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
			var diffDays = Math.round(Math.abs((_firstDate.getTime() - _secondDate.getTime()) / (oneDay)));
			if (diffDays > 30) {
				var diffMonth = Math.round(diffDays / 30);
				if (diffMonth >= 12) {
					var diffYear = Math.round(diffMonth / 12);
					str += diffYear;
					str += (diffYear == 1) ? ' año' : ' años';
					diffMonth = diffMonth - (diffYear * 12);
					if (diffMonth > 0) {
						str += ' y ';
						str += diffMonth;
						str += (diffMonth == 1) ? ' mes' : ' meses';
					}
				} else {
					str += diffMonth;
					str += (diffMonth == 1) ? ' mes' : ' meses';
				}
			} else {
				str += diffDays;
				str += (diffDays == 1) ? ' día' : ' días';
			}
			return str;
		}
		return false;
	};
	// Function to search to autocomplete skills
	$rootScope.searchAutocomplete = function (_str, _array) {
		var matches = [];
		_array.forEach(function (item) {
			if (item.name.toLowerCase().indexOf(_str.toString().toLowerCase()) >= 0) {
				matches.push(item);
			}
		});
		matches = matches.splice(0, 5);
		matches.sort(function (a, b) {
			if (a.name < b.name)
				return -1;
			if (a.name > b.name)
				return 1;
			return 0;
		});
		return matches;
	};
	// Function when we select a skill on autocomplete, add to array skills
	$rootScope.selectedSkill = function (_item) {
		var array = $rootScope.item.skills;
		if (array) {
			var exist = false;
			for (var i = 0; i < array.length; i++) {
				if (array[i] == _item.title) {
					exist = true;
				}
			}
			if (!exist)
				array.push(_item.title);
		} else {
			$rootScope.item.skills = [];
			$rootScope.item.skills.push(_item.title);
		}
	};
	// Function to detect Chrome
	$rootScope.isChrome = function () {
		return isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	};
	// Function to detect Firefox
	$rootScope.isFirefox = function () {
		return isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
	};
	// Function toogle type of input between text and password
	$rootScope.toogleTypeInput = function ($event) {
		var input = $($event.target).parent().parent().find('input');
		var type = input.attr("type");
		if (type == 'password') {
			input.attr('type', 'text');
		} else if (type == 'text') {
			input.attr('type', 'password');
		}
	};
	/*****************/
	/* AUXILIAR FUNCTIONS */
	/*****************/


	/*********************/
	/* ROLES FUNCTIONS   */
	/*********************/
	// Function to check if user is superuser
	$rootScope.checkSuperUser = function () {
		if (!$rootScope.isSuperUser()) {
			// Load user profile because delay request profile
			var item = {'id': $rootScope.globals.currentUser.id};
			App.read('profiles', item).then(function (data) {
				if (App.responseOK(data)) {
					if (data.response.id_rol > 1) {
						$location.path('/');
						$rootScope.selectSection('dashboard');
					}
				}
			});
		}
	};
	// Function to check if user is admin
	$rootScope.checkAdmin = function () {
		if (!$rootScope.isAdmin()) {
			// Load user profile because delay request profile
			var item = {'id': $rootScope.globals.currentUser.id};
			App.read('profiles', item).then(function (data) {
				if (App.responseOK(data)) {
					if (data.response.id_rol > 2) {
						$location.path('/');
						$rootScope.selectSection('dashboard');
					}
				}
			});
		}
	};
	// Function to check if user is superuser
	$rootScope.isSuperUser = function () {
		return $rootScope.checkLevelUser(1);
	};
	// Function to check if user is admin
	$rootScope.isAdmin = function () {
		return $rootScope.checkLevelUser(2);
	};
	// Function auxiliar to check if level user
	$rootScope.checkLevelUser = function (_n) {
		if ($rootScope.storage['profile'] != undefined &&
			($rootScope.storage['profile']['id_rol'] <= _n)) {
			return true;
		} else {
			return false;
		}
	};
	// Function to logout user
	$rootScope.logout = function () {
		App.Logout();
		$scope.isLocked = false;
		App.encryptedLS('dekcol', 'false');
		$location.path('/login');
	};
	/*********************/
	/* ROLES FUNCTIONS   */
	/*********************/


	/************************/
	/* CALENDAR FUNCTIONS   */
	/************************/
	$rootScope.toEventCalendar = function (_item) {
		if (_item.active == 1) {
			var event =
				{
					id: _item.id,
					title: _item.description,
					start: dateToCalendar(_item.datetime_start),
					end: dateToCalendar(_item.datetime_end)
				};
		} else {
			// If item is desactive, set date default to not show on calendar
			var event =
				{
					id: _item.id,
					title: '',
					start: DATE_DEFAULT,
					end: DATE_DEFAULT
				};
		}
		return event;
	};

//	d/M/Y H:m  <==> Y-M-d+"T"+H:m
	function dateToCalendar(_date) {
		if (_date) {
			var arrayDateTime = _date.split(' ');
			var date = arrayDateTime[0];
			var time = arrayDateTime[1];
			if (date.indexOf('-') > -1) {
				var arrayDate = date.split('-');
			} else {
				var arrayDate = date.split('/');
			}
			return arrayDate[2] + '-' + arrayDate[1] + '-' + arrayDate[0] + 'T' + time + ':00';
		}
		return DATE_DEFAULT;
	}
	/************************/
	/* CALENDAR FUNCTIONS   */
	/************************/
});
/******************************* END APP CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

