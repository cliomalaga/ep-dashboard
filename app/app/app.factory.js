/******************************* APP FACTORY ************************************************************/
app.factory('App', function ($http, $q, $rootScope, $location, $cookies, URL_PHP, KEY_LS, DELAY_NOTIFICATION, toaster) {
	return{
		/************/
		/* LOGIN    */
		/************/
		Login: function (_user) {
			return this.sendRequest("login", _user);
		},
		Logout: function () {
			this.sendSession({action: "delete-session"}); // Delete session to hide templates
			this.ClearCredentials();
		},
		SetCredentials: function (id, email, password, token) {
			var authdata = Base64.encode(email + ':' + password);
			var authtoken = Base64.encode(token);
			$rootScope.globals = {
				currentUser: {
					id: id,
					authdata: authdata,
					authtoken: authtoken
				}
			};
//			this.sendSession({action: "create-session"}); // Create session to show templates
			$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
			var expireDate = new Date();
			expireDate.setDate(expireDate.getDate() + 1);// Find tomorrow's date.
			$cookies.put('authenticate', angular.toJson($rootScope.globals), {'expires': expireDate});
		},
		ClearCredentials: function () {
			$rootScope.globals = {};
			$cookies.remove('authenticate');
			$http.defaults.headers.common.Authorization = 'Basic';
		},
		getToken: function () {
			$rootScope.globals = angular.fromJson($cookies.get('authenticate')) || {};
			if ($rootScope.globals && $rootScope.globals.currentUser && $rootScope.globals.currentUser.authtoken) {
				var encodedToken = $rootScope.globals.currentUser.authtoken;
				return Base64.decode(encodedToken);
			}
		},
		encryptedLS: function (_key, _value) {
			var encrypted = CryptoJS.AES.encrypt(_value, KEY_LS);
			localStorage.setItem(_key, angular.fromJson(encrypted));
		},
		decryptedLS: function (_key) {
			var decrypt = '';
			if (localStorage.getItem(_key)) {
				var decrypted = CryptoJS.AES.decrypt(localStorage.getItem(_key), KEY_LS);
				decrypt = decrypted.toString(CryptoJS.enc.Utf8);
			}
			return decrypt;
		},
		/************/
		/* MODAL CONFIRM DELETE */
		/************/
		modalConfirm: function (_text, _textButton) {
			var defered = $q.defer();
			var promise = defered.promise;
			try {
				$rootScope.isBlur = true;
				if (!_text) {
					_text = '¿Seguro que quiere eliminar?';
				}
				if (!_textButton) {
					_textButton = '<i class="entypo-cancel"></i> Eliminar';
				}
				// Show dialog "Sweetalert"
				swal({
					title: _text,
					type: 'error',
					showCancelButton: true,
					cancelButtonText: '<i class="entypo-ccw"></i> Cancelar',
					confirmButtonClass: "btn-danger",
					confirmButtonText: _textButton,
					closeOnConfirm: true,
					animation: false,
					customClass: 'animated zoomIn',
					allowOutsideClick: false
				}).then(function () {
					$rootScope.isBlur = false;
					defered.resolve(true);
				}, function (dismiss) {
					$rootScope.isBlur = false;
					defered.resolve(false);
				})
			} catch (e) {
				defered.reject(e);
			}
			return promise;
		},
		/****************/
		/* RESPONSES    */
		/****************/
		existNotification: function (_message) {
			var arrayNotification = $('#toast-container .toast-title');
			for (var i = 0; i < arrayNotification.length; i++) {
				var notificationText = $(arrayNotification[i]).text();
				if (notificationText.length > 0) {
					if (_message.toUpperCase() === notificationText.toUpperCase()) {
						return true;
					}
				}
			}
			return false;
		},
		notification: function (_status, _data) {
			// Get timeout notification
			if ($rootScope.configuration && $rootScope.configuration.delay_notification) {
				var timeout = $rootScope.configuration.delay_notification * 1000;
			} else {
				var timeout = DELAY_NOTIFICATION * 1000;
			}
			// Set message text
			var message = '';
			if (_data.message) {
				message = _data.message;
			} else if (_data.response) {
				message = _data.response;
			} else if (_data.error) { // Usually "Token expired"
				// Prevent duplicates notifications if error is "token expired"
				if (!this.existNotification(_data.error)) {
					message = _data.error;
					$rootScope.logout();
				}
			} else {
				message = 'Ha habido un error';
			}
			// Display notification if exist message
			if (message.length > 0) {
				var options = {
					body: message,
					timeout: timeout
				};
				if (_status == 'OK') {
					toaster.success(options);
				} else if (_status == 'KO') {
					toaster.error(options);
				}
			}
		},
		responseOK: function (_data) {
			// Function to handle response API
			if (_data.ok) {
				if (_data.message) {
					this.notification('OK', _data);
				}
				return true;
			} else {
				this.notification('KO', _data);
				if (_data.code == '404') {
					$rootScope.redirect($rootScope.actualSection.nameUrl);
				}
				return false;
			}
		},
		/************/
		/* REQUESTS */
		/************/
		read: function (_section, _item) {
			return this.sendRouter({action: 'read-' + _section, item: _item});
		},
		create: function (_section, _item) {
			return this.sendRouter({action: 'create-' + _section, item: _item});
		},
		update: function (_section, _item) {
			return this.sendRouter({action: 'update-' + _section, item: _item});
		},
		delete: function (_section, _item) {
			return this.sendRouter({action: 'delete-' + _section, item: _item});
		},
		activateDesactivate: function (_section, _item) {
			var action = 'activate';
			if ($rootScope.isActive(_item)) {
				action = 'desactivate';
			}
			return this.sendRouter({action: action + '-' + _section, item: _item});
		},
		sendEmails: function (_item) {
			return this.sendRouter({action: 'send-emails', item: _item});
		},
		sendRequest: function (_action, _item) {
			return this.sendRouter({action: _action, item: _item});
		},
		sendRouter: function (_params) {
			var defered = $q.defer();
			var promise = defered.promise;
			try {
				_params.token = this.getToken();
				$http.post(URL_PHP + 'router.php', _params)
					.success(function (data) {
						defered.resolve(data);
					})
					.error(function (error) {
						defered.reject(error);
					});
			} catch (e) {
				defered.reject(e);
			}
			return promise;
		},
		sendSession: function (_params) {
			var defered = $q.defer();
			var promise = defered.promise;
			try {
				$http.post(URL_PHP + 'session.php', _params)
					.success(function (data) {
						defered.resolve(data);
					})
					.error(function (error) {
						defered.reject(error);
					});
			} catch (e) {
				defered.reject(e);
			}
			return promise;
		},
		/************/
		/* UPLOADS */
		/************/
		uploadFile: function (_files) {
			var deferred = $q.defer();
			var formData = new FormData();
			formData.append("files", _files);
			console.log(FormData);
			return $http.post("php/server.php", formData, {
				headers: {
					"Content-type": undefined
				},
				transformRequest: angular.identity
			})
				.success(function (res)
				{
					deferred.resolve(res);
				})
				.error(function (msg, code)
				{
					deferred.reject(msg);
				});
			return deferred.promise;
		}
	};
});



// Base64 encoding service used by AuthenticationService
var Base64 = {
	keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
	encode: function (input) {
		var output = "";
		var chr1, chr2, chr3 = "";
		var enc1, enc2, enc3, enc4 = "";
		var i = 0;
		do {
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
			output = output +
				this.keyStr.charAt(enc1) +
				this.keyStr.charAt(enc2) +
				this.keyStr.charAt(enc3) +
				this.keyStr.charAt(enc4);
			chr1 = chr2 = chr3 = "";
			enc1 = enc2 = enc3 = enc4 = "";
		} while (i < input.length);
		return output;
	},
	decode: function (input) {
		var output = "";
		var chr1, chr2, chr3 = "";
		var enc1, enc2, enc3, enc4 = "";
		var i = 0;
		// Remove all characters that are not A-Z, a-z, 0-9, +, /, or =
		var base64test = /[^A-Za-z0-9\+\/\=]/g;
		if (base64test.exec(input)) {
			window.alert("There were invalid base64 characters in the input text.\n" +
				"Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
				"Expect errors in decoding.");
		}
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
		do {
			enc1 = this.keyStr.indexOf(input.charAt(i++));
			enc2 = this.keyStr.indexOf(input.charAt(i++));
			enc3 = this.keyStr.indexOf(input.charAt(i++));
			enc4 = this.keyStr.indexOf(input.charAt(i++));
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
			output = output + String.fromCharCode(chr1);
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
			chr1 = chr2 = chr3 = "";
			enc1 = enc2 = enc3 = enc4 = "";
		} while (i < input.length);
		return output;
	}
};