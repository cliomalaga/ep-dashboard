<?php // @include ('../../php/checkLogged.php');                                                                                                                           ?>


<div class="sidebar-menu fixed">
	<div class="sidebar-menu-inner">
		<header class="logo-env">
			<!-- logo -->
			<div class="logo-background">
				<img src="{{ storage.profile.avatar}}" alt="{{ storage.profile.name}} {{ storage.profile.lastname}}"/>
			</div>
			<div id="logo" class="logo">
				<img resizeimgresponsive src="{{ storage.profile.avatar}}" alt="{{ storage.profile.name}} {{ storage.profile.lastname}}" />
			</div>
			<div class="logo-info">
				<h1>{{ storage.profile.name}}</h1>
				<h2>{{ storage.profile.lastname}}</h2>
			</div>


			<div class="sidebar-mobile-menu button-menu-responsive">
				<a ng-click="$root.isMenuResponsiveOpen = !$root.isMenuResponsiveOpen" class="with-animation">
					<i class="entypo-menu"></i>
				</a>
			</div>
		</header>


		<ul id="main-menu" class="main-menu animated"
			ng-class="{'open':$root.isMenuResponsiveOpen}">
			<li ng-class="{active : isActiveSection('dashboard')}">
				<a ui-sref="dashboard" ng-click="selectSection('dashboard')">
					<i class="entypo-address"></i>
					<span class="title">Dashboard</span>
				</a>
			</li>

			<li ng-class="{active : isActiveSection('perfil')}">
				<a ui-sref="perfil" ng-click="selectSection('perfil')">
					<i class="entypo-user"></i>
					<span class="title">Perfil</span>
				</a>
			</li>


			<li class="has-sub" ng-class="{'opened': isActiveSection(['menu-ep','pruebas','preguntas','documentos','ensenanzas','casos','investigaciones'])}">
				<a ng-click="selectSection('menu-ep', true)">
					<i class="icon-man"></i>
					<span class="title">Evaluación Psicológica</span>
				</a>

				<ul ng-class="{'visible opened': isActiveSection(['menu-ep','pruebas','preguntas','documentos','ensenanzas','casos','investigaciones'])}">

					<li ng-click="selectSection('pruebas')" ng-class="{active : isActiveSection('pruebas')}">
						<a ui-sref="pruebas">
							<i class="entypo-traffic-cone"></i>
							<span class="title">Pruebas</span>
						</a>
					</li>
					<li ng-click="selectSection('preguntas')" ng-class="{active : isActiveSection('preguntas')}">
						<a ui-sref="preguntas">
							<i class="entypo-help"></i>
							<span class="title">Preguntas</span>
						</a>
					</li>
					<li ng-click="selectSection('documentos')" ng-class="{active : isActiveSection('documentos')}">
						<a ui-sref="documentos">
							<i class="entypo-folder"></i>
							<span class="title">Documentos</span>
						</a>
					</li>
					<li ng-click="selectSection('ensenanzas')" ng-class="{active : isActiveSection('ensenanzas')}">
						<a ui-sref="ensenanzas">
							<i class="entypo-graduation-cap"></i>
							<span class="title">Enseñanzas</span>
						</a>
					</li>
					<li ng-click="selectSection('casos')" ng-class="{active : isActiveSection('casos')}">
						<a ui-sref="casos">
							<i class="entypo-book"></i>
							<span class="title">Casos</span>
						</a>
					</li>
					<li ng-click="selectSection('investigaciones')" ng-class="{active : isActiveSection('investigaciones')}">
						<a ui-sref="investigaciones">
							<i class="icon-pie-chart"></i>
							<span class="title">Investigaciones</span>
						</a>
					</li>

				</ul>
			</li>


			<!--If user is admin-->
			<li ng-if="isAdmin()" class="has-sub" ng-class="{'opened': isActiveSection(['menu-agenda','calendario','eventos'])}">
				<a ng-click="selectSection('menu-agenda', true)">
					<i class="icon-open-book"></i>
					<span class="title">Agenda</span>
				</a>
				<ul ng-class="{'visible opened': isActiveSection(['menu-agenda','calendario','eventos'])}">
					<li  ng-click="selectSection('calendario')" ng-class="{'active':  isActiveSection('calendario')}">
						<a ui-sref="calendario">
							<i class="entypo-calendar"></i>
							<span class="title">Calendario</span>
						</a>
					</li>
					<li  ng-click="selectSection('eventos')" ng-class="{'active':  isActiveSection('eventos')}">
						<a ui-sref="eventos">
							<i class="icon-pin"></i>
							<span class="title">Eventos</span>
						</a>
					</li>
				</ul>
			</li>


			<!--If user is not admin-->
			<li ng-if="!isAdmin()" ng-class="{active : isActiveSection('calendario')}">
				<a ui-sref="calendario" ng-click="selectSection('calendario')">
					<i class="entypo-calendar"></i>
					<span class="title">Calendario</span>
				</a>
			</li>


			<!--If user is admin-->
			<li ng-if="isAdmin()" class="has-sub" ng-class="{'opened': isActiveSection(['menu-mensajes','mensajes','mensajes/enviar'])}">
				<a ng-click="selectSection('menu-mensajes', true)">
					<i class="entypo-mail"></i>
					<span class="title">Mensajes</span>
				</a>
				<ul ng-class="{'visible opened': isActiveSection(['menu-mensajes','mensajes','mensajes/enviar'])}">
					<li  ng-click="selectSection('mensajes/enviar')" ng-class="{'active':  isActiveSection('mensajes/enviar')}">
						<a ui-sref="mensajes/enviar">
							<i class="entypo-export"></i>
							<span class="title">Enviar mensaje</span>
						</a>
					</li>
					<li  ng-click="selectSection('mensajes')" ng-class="{'active':  isActiveSection('mensajes')}">
						<a ui-sref="mensajes">
							<i class="entypo-upload"></i>
							<span class="title">Mensajes enviados</span>
						</a>
					</li>
				</ul>
			</li>


			<!--If user is superroot-->
			<li ng-if="isSuperUser()" ng-class="{active : isActiveSection('usuarios')}">
				<a ui-sref="usuarios" ng-click="selectSection('usuarios')">
					<i class="icon-users"></i>
					<span class="title">Usuarios</span>
				</a>
			</li>


			<!--If user is superroot-->
			<li ng-if="isSuperUser()" ng-class="{active : isActiveSection('categorias')}">
				<a ui-sref="categorias" ng-click="selectSection('categorias')">
					<i class="entypo-bookmarks"></i>
					<span class="title">Categorías</span>
				</a>
			</li>
			
			
			<li ng-class="{active : isActiveSection('configuracion')}">
				<a ui-sref="configuracion" ng-click="selectSection('configuracion')">
					<i class="entypo-tools"></i>

					<span class="title">Configuración</span>
				</a>
			</li>


			<li>
				<a ng-click="logout()">
					<i class="entypo-fast-backward"></i>
					<span class="title">Salir</span>
				</a>
			</li>
		</ul>
	</div>
</div>


