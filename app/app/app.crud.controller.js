/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* APP CONTROLLER ************************************************************/
app.controller("CrudController", function ($rootScope, $scope, $location, $timeout, App) {
	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.item = {};
	$rootScope.itemOriginal = {};
	/******************/
	/* END VARIABLES  */
	/******************/


	/**************************/
	/* BUTTON LIST FUNCTIONS */
	/*************************/
	// View detail of item to edit
	$scope.edit = function (_item) {
		$rootScope.redirect($rootScope.actualSection.itemName + '/' + _item.id);
	};

	// Active or desactivate item
	$scope.activateDesactivate = function (_item) {
		var array = $rootScope.storage[$rootScope.actualSection.nameDB];
		var i = $rootScope.findInArray(_item, array);
		// Activate or desactivate in DB
		App.activateDesactivate($rootScope.actualSection.nameDB, _item).then(function (data) {
			// If request is success
			if (App.responseOK(data)) {
				// Activate or desactivate in local
				_item.active = data.response.active;
				array[i].active = data.response.active;
				// If edit event, update in events to update Calendar
				$scope.updateEventsCalendar(data);
			}
		});
	};
	/**************************/
	/* BUTTON LIST FUNCTIONS */
	/*************************/



	/**************************/
	/* BUTTON ITEM FUNCTIONS */
	/*************************/
	// Function to restore original item
	$scope.undo = function (_item) {
		var array = $rootScope.storage[$rootScope.actualSection.nameDB];
		var i = $rootScope.findInArray(_item, array);
		$rootScope.item = angular.copy($rootScope.itemOriginal);
		array[i] = angular.copy($rootScope.itemOriginal);
		if ($rootScope.actualSection.itemDB == 'configuration') {
			$rootScope.setConfiguration();
		}
	};

	// Function to restore original item and back to list view
	$scope.back = function (_item) {
		var array = $rootScope.storage[$rootScope.actualSection.nameDB];
		var i = $rootScope.findInArray(_item, array);
		array[i] = angular.copy($rootScope.itemOriginal);
		// Clean item and redirect
		$rootScope.item = {};
		$rootScope.itemOriginal = {};
		$rootScope.redirect($rootScope.actualSection.nameUrl);
	};
	/**************************/
	/* BUTTON ITEM FUNCTIONS */
	/*************************/



	/**************************/
	/* CRUD FUNCTIONS */
	/*************************/
	$rootScope.readList = function () {
		var array = $rootScope.storage[$rootScope.actualSection.nameDB];
		if (array) {
			// Load items from Local
			$rootScope.collection = angular.copy(array);
		} else {
			// Load items from DB
			App.read($rootScope.actualSection.nameDB, null).then(function (data) {
				if (App.responseOK(data)) {
					$rootScope.collection = angular.copy(data.response);
					$rootScope.storage[$rootScope.actualSection.nameDB] = angular.copy(data.response);
				}
			});
		}
	};

	$rootScope.readItem = function () {
		var array = $rootScope.storage[$rootScope.actualSection.nameDB];
		if (array) {
			// Load item from Local
			if ($rootScope.actualSection.itemDB == 'profiles') {
				var i = $rootScope.findInArrayByUser($rootScope.item, array);
			} else {
				var i = $rootScope.findInArray($rootScope.item, array);
			}
			$rootScope.item = angular.copy(array[i]);
			$rootScope.itemOriginal = angular.copy(array[i]);
		} else {
			// Load item from DB
			App.read($rootScope.actualSection.itemDB, $rootScope.item).then(function (data) {
				if (App.responseOK(data)) {
					$rootScope.item = angular.copy(data.response);
					$rootScope.itemOriginal = angular.copy(data.response);
				}
			});
		}
	};


	// Create item
	$scope.create = function () {
		$rootScope.redirect($rootScope.actualSection.itemName + '/');
	};


	// Function to update changes if item is editing
	$scope.save = function (_item, _redirect = true) {
		var array = $rootScope.storage[$rootScope.actualSection.nameDB];
		if (!_item.id) {
			// Create item in DB
			App.create($rootScope.actualSection.nameDB, _item).then(function (data) {
				console.log(data);
				// If request is success
				if (App.responseOK(data)) {
					// If create event, add to events to update Calendar
					$scope.createEventsCalendar(data);
					// Create item in local
					array.unshift(data.response);
					$rootScope.item = angular.copy(data.response);
					$rootScope.itemOriginal = angular.copy(data.response);
					$rootScope.collection = angular.copy(array);
					$rootScope.storage[$rootScope.actualSection.nameDB] = angular.copy(array);
					if (_redirect) {
						$location.path($rootScope.actualSection.itemName + '/' + data.response.id);
					}
				}
			});
		} else { // If item is editing
			// Update item in DB
			App.update($rootScope.actualSection.nameDB, _item).then(function (data) {
				// If edit configuration, we set configuration on web (Before response beacuse the response become with new configuration)
				$scope.updateConfiguration(data);
				//	If request is success
				if (App.responseOK(data)) {
					// If edit user own, update in users and profiles arrays to other sections
					$scope.updateUserOwn(data);
					// If edit event, update in events to update Calendar
					$scope.updateEventsCalendar(data);
					// Update item in local
					$rootScope.item = angular.copy(data.response);
					$rootScope.itemOriginal = angular.copy(data.response);
					var i = $rootScope.findInArray(data.response, array);
					array[i] = angular.copy(data.response);
				}
			});
	}
	};


	// Function to delete item
	$scope.delete = function (_item) {
		var array = $rootScope.storage[$rootScope.actualSection.nameDB];
		var i = $rootScope.findInArray(_item, array);
		// Open modal to confirm delete
		App.modalConfirm().then(function (confirm) {
			// If confirm delete
			if (confirm) {
				// Delete in database
				App.delete($rootScope.actualSection.nameDB, _item).then(function (data) {
					// If request is success
					if (App.responseOK(data)) {
						// Delete in local
						array.splice(i, 1);
						$rootScope.collection.splice(i, 1);
						// If delete event, delete to Calendar too
						$scope.deleteEventsCalendar(_item);
					}
				});
			}
		});
	};


	// Function to update own profile user
	$scope.updateUserOwn = function (_data) {
		if ($rootScope.actualSection.itemDB == 'profiles' ||
			$rootScope.actualSection.itemDB == 'user' &&
			($rootScope.item.id == $rootScope.globals.currentUser.id))
		{
			if (App.decryptedLS('rebmemer') && $rootScope.item.pnew) {
				App.encryptedLS('drowssap', $rootScope.item.pnew);
			}
			if ($rootScope.storage.users) {
				var i = $rootScope.findInArrayByUser($rootScope.item, $rootScope.storage.users);
				$rootScope.storage.users[i] = angular.copy(_data.response);
			}
			var i = $rootScope.findInArrayByUser($rootScope.item, $rootScope.storage.profiles);
			$rootScope.storage.profiles[i] = angular.copy(_data.response);
			$rootScope.storage.profile = angular.copy(_data.response);
			// If user is not superuser after editing, user redirect to home
			// $rootScope.checkSuperUser();
		}
	};


	// Function to update Configuration
	$scope.updateConfiguration = function (_data) {
		if (_data.ok) {
			if ($rootScope.actualSection.itemDB == 'configuration') {
				$rootScope.storage.configurations[0] = angular.copy(_data.response);
				$rootScope.storage.configuration = angular.copy(_data.response);
				$rootScope.setConfiguration();
			}
		}
	};


	// Function to create events to Calendar
	$scope.createEventsCalendar = function (_data) {
		if (_data.ok) {
			if ($rootScope.actualSection.itemDB == 'event') {
				$rootScope.events.unshift($rootScope.toEventCalendar(_data.response));
			}
		}
	};


	// Function to update events to Calendar
	$scope.updateEventsCalendar = function (_data) {
		if (_data.ok) {
			if ($rootScope.actualSection.itemDB == 'event') {
				var i = $rootScope.findInArray(_data.response, $rootScope.events);
				$rootScope.events[i] = angular.copy($rootScope.toEventCalendar(_data.response));
			}
		}
	};

	// Function to delete events to calendar
	$scope.deleteEventsCalendar = function (_item) {
		if ($rootScope.actualSection.itemDB == 'event') {
			var i = $rootScope.findInArray(_item, $rootScope.events);
			$rootScope.events.splice(i, 1);
		}
	};

	// Function to delete to array skills
	$rootScope.deleteLabel = function (_str, _array) {
		for (var i = 0; i < _array.length; i++) {
			if (_array[i] == _str) {
				_array.splice(i, 1);
				break;
			}
		}
		if (!_array) {
			_array = [];
		}
	};
	/**************************/
	/* CRUD FUNCTIONS */
	/*************************/
});
/******************************* END APP CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

