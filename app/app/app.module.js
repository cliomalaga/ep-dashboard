var app = angular.module('Prueba',
	['ui.router',
		'angular.less',
		'ngCookies',
		'ngAnimate',
		'ngIdle',
		'toaster',
		'kendo.directives',
		'ngTextTruncate',
		'angucomplete-alt',
		'rzModule',
		'uiSwitch',
		'minicolors',
		'ngtimeago',
		'textAngular'
	]);

