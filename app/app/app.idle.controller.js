/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* APP CONTROLLER ************************************************************/
app.controller("IdleController", function ($rootScope, $scope, $cookies, App) {
	/*******************/
	/* START IDLE */
	/*******************/
	// When user has time out inactivty
	$rootScope.$on('$idleTimeout', function () {
		$scope.isLocked = true;
		App.encryptedLS('dekcol', 'true');
	});
	// Each moment of activity, locked o disblocked app according inactivy in other tabs automatically
	$rootScope.$on('$keepalive', function () {
		$rootScope.globals = angular.fromJson($cookies.get('authenticate')) || {};
		// If exist user logued
		if ($rootScope.globals.currentUser) {
			$rootScope.forceLogin();
			if (App.decryptedLS('dekcol') == 'true') { // Acording LS boolean
				$scope.isLocked = true;
			} else {
				$scope.isLocked = false;
			}
		} else {
			// If not exist user logued
			$rootScope.logout();
		}
	});
	$rootScope.$on('$idleWarn', function (e, countdown) {
		console.log(countdown);
	});

	// Function to login user on locked screen
	$scope.loginLocked = function () {
		$scope.dataLoading = true;
		$rootScope.user.email = $rootScope.storage.profile.email;
		App.Login($rootScope.user).then(function (data) {
			// Check same user in this session
			if (App.responseOK(data) && (data.response.user.id == $rootScope.globals.currentUser.id)) {
				$rootScope.user.password = '';
				$scope.isLocked = false;
				App.encryptedLS('dekcol', 'false');
			}
			$scope.dataLoading = false;
		});
	};
	/*******************/
	/* END IDLE */
	/*******************/
});
/******************************* END APP CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

