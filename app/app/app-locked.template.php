<div class="login-container">

	<div class="login-header">

		<div class="login-content">

			<img ng-src="assets/images/logo-full.png" class="logo-locked"/>
			<p class="description">Introduzca su contraseña para quitar el bloqueo de la pantalla</p>
		</div>

	</div>

	<div class="locked-form">

		<div class="locked-content">

			<form ng-submit="loginLocked()" name="form" role="form" class="form-primary">
				<div class="form-group lockscreen-input">
					<div class="lockscreen-thumb">
						<img ng-src="{{ storage.profile.avatar}}" class=" avatar">
					</div>
				</div>
				<div class="form-group form-entry display-inline-block">
					<label for="email" class="control-label">Usuario actual</label>
					<input type="text" class="form-control" readonly="" value="{{ storage.profile.name}} {{ storage.profile.lastname}}"/>
				</div>
				<div class="form-group form-entry display-inline-block" ng-class="{
				'has-error'
				: form.password.$dirty && form.password.$error.required }">
					<label for="password" class="control-label">Contraseña</label>
					<input type="password" name="password" id="password" class="form-control" ng-model="user.password" required />
				</div>
				<div class="form-group  form-entry text-right">
					<button type="submit" ng-disabled="form.$invalid || dataLoading" class="press-button btn btn-item btn-primary-inverse">
						<i ng-if="!dataLoading" class="entypo-login"></i>
						<i ng-if="dataLoading" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
						Login
					</button>
				</div>
				<div class="form-group  form-entry text-right">
					<button type="button" class="press-button btn btn-item btn-primary-inverse" ng-click="logout()">
						<i class="entypo-logout"></i>
						Salir
					</button>
				</div>
			</form>



		</div>
	</div>
</div>