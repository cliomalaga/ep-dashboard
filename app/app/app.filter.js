app.filter('capitalize', function () {
	return function (input) {
		return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
	}
});


app.filter('sortItemBy', function () {
	return function (items, field, reverse) {
		var filtered = [];
		angular.forEach(items, function (item) {
			filtered.push(item);
		});

		if (field == null) {
			filtered = filtered;
		} else if (field == 'active') {
			filtered.sort(function (a, b) {
				return (parseFloat(a[field]) < parseFloat(b[field]) ? 1 : -1);
			});
		} else if (field == 'datetime_start' || field == 'datetime_end' || field == 'created_at') {
			filtered.sort(function (a, b) {
				function parseDate(_date) {
					var separator = (_date.indexOf('/') > 0) ? '/' : '-';
					if (_date) {
						var parts = _date.split(separator);
						return new Date(parts[1] + separator + parts[0] + separator + parts[2]); // Note: months are 0-based
					}
					return new Date(_date);
				}
				var dateA = parseDate(a[field]);
				var dateB = parseDate(b[field]);
				return (dateA.getTime() > dateB.getTime() ? 1 : -1);
			});
		} else {
			filtered.sort(function (a, b) {
				if (a[field] && b[field]) {
					return (a[field].toLowerCase() > b[field].toLowerCase() ? 1 : -1);
				}
			});
		}

		if (reverse)
			filtered.reverse();

		return filtered;
	};
});


app.filter("emptyToEnd", function () {
	return function (array, key) {
		if (!angular.isArray(array))
			return;
		var present = array.filter(function (item) {
			return item[key];
		});
		var empty = array.filter(function (item) {
			return !item[key]
		});
		return present.concat(empty);
	};
});


app.filter('deleteArticle', function () {
	return function (input) {
		input = input || '';
		if (input.length > 0) {
			var array = input.split(' ');
			if (array.length > 1) {
				input = array[1];
			}
		}
		return input;
	};
});


app.filter('onlyCategory', function () {
	return function (items, category) {
		var filtered = [];
		// If category is String
		if (category && category.length > 0) {
			var categoryCapitalize = category[0].toUpperCase() + category.slice(1);
			angular.forEach(items, function (item) {
				if (item.category_name == categoryCapitalize) {
					filtered.push(item);
				}
			});
			return filtered;
		} else {
			// If category is Number
			if (Number.isInteger(category)) {
				angular.forEach(items, function (item) {
					if (item.id_category == category) {
						filtered.push(item);
					}
				});
				return filtered;
			}
		}
	};
});

app.filter('onlyAttachedName', function () {
	return function (item) {
		if (item) {
			if (item.indexOf('/') > 0) {
				var arrayFile = item.split('/');
				var nameFile = arrayFile[arrayFile.length - 1];
				if (nameFile.indexOf('_') > 0) {
					var arrayName = item.split('_');
					arrayName.shift();
					var name = arrayName.join('_');
					return name;
				}
			}
			return item;
		}
	};
});