/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* APP CONTROLLER ************************************************************/
app.controller("AppController", function ($controller, $rootScope, $scope, $location, $timeout, $idle, App, DELAY_IDLE) {
	$rootScope.splash = true;
	$timeout(function () {
		$rootScope.splash = false;
	}, 2000);


	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.user = {};
	$rootScope.storage = [];
	$scope.isBlur = false;
	$rootScope.isMenuResponsiveOpen = false;
	$rootScope.calendarOptions = {
		"culture": "es-ES",
		"k-format": "dd/MM/yyyy"
	};
	$rootScope.configuration = {};
	$rootScope.configuration.animations = true;
	$rootScope.configuration.theme_basic = false;
	$rootScope.configuration.views = ['grid', 'list', 'table'];
	$rootScope.configuration.viewsCalendar = ['month', 'agendaWeek', 'listWeek'];
	if (App.decryptedLS('dekcol') == 'true') {
		$scope.isLocked = true;
	} else {
		$scope.isLocked = false;
	}
	/******************/
	/* END VARIABLES  */
	/******************/



	/*******************/
	/* START LOAD IF LOGGED  */
	/*******************/
	if ($rootScope.globals.currentUser) {
		/****************************/
		/* START INYECT CONTROLLER */
		/****************************/
		$controller('AuxiliarController', {$scope: $scope});
		$controller('CrudController', {$scope: $scope});
		$controller('IdleController', {$scope: $scope});
		/****************************/
		/****************************/
		/* END INYECT CONTROLLER */
		/****************************/



		/*******************/
		/* ONLOAD FUNCTIONS */
		/*******************/
		//  Selected section on menu
		if ($location.path().length > 1) {
			$scope.selected = $location.path().replace('/', '');
		} else {
			$scope.selected = 'dashboard';
		}
		$rootScope.selectSection($scope.selected);
		// Load user roles
		App.read('roles', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['roles'] = angular.copy(data.response);
			}
		});
		// Load user profile
		var item = {'id': $rootScope.globals.currentUser.id};
		App.read('profiles', item).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['profiles'] = [];
				$rootScope.storage['profiles'].push(data.response);
				$rootScope.storage['profile'] = data.response;
				// Load configuration
				App.read('configuration', null).then(function (data) {
					if (App.responseOK(data)) {
						$rootScope.storage['configurations'] = [];
						$rootScope.storage['configurations'].push(data.response);
						$rootScope.storage['configuration'] = data.response;
						$rootScope.setConfiguration();
					}
				});
				// Load users if user is admin
				if ($rootScope.isAdmin()) {
					App.read('users', null).then(function (data) {
						if (App.responseOK(data)) {
							$rootScope.storage['users'] = angular.copy(data.response);
						}
					});
					App.read('emails', null).then(function (data) {
						if (App.responseOK(data)) {
							$rootScope.storage['emails'] = angular.copy(data.response);
						}
					});
				}
			}
		});
		// Load categories
		App.read('categories', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['categories'] = angular.copy(data.response);
			}
		});
		// Load subcategories
		App.read('subcategories', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['subcategories'] = angular.copy(data.response);
			}
		});
		// Load tests
		App.read('tests', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['tests'] = angular.copy(data.response);
			}
		});
		// Load faqs
		App.read('faqs', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['faqs'] = angular.copy(data.response);
			}
		});
		// Load documents
		App.read('documents', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['documents'] = angular.copy(data.response);
			}
		});
		// Load faqs
		App.read('teachings', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['teachings'] = angular.copy(data.response);
			}
		});
		// Load cases
		App.read('cases', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['cases'] = angular.copy(data.response);
			}
		});
		// Load investigations
		App.read('investigations', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['investigations'] = angular.copy(data.response);
			}
		});
		// Load events
		App.read('events', null).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.storage['events'] = angular.copy(data.response);
				$rootScope.events = [];
				for (var i = 0; i < $rootScope.storage['events'].length; i++) {
					var item = $rootScope.storage['events'][i];
					$rootScope.events.push($rootScope.toEventCalendar(item));
				}
			}
		});
		/*******************/
		/* ONLOAD FUNCTIONS */
		/*******************/



		/*******************/
		/* FUNCTIONS       */
		/*******************/
		// Function to set Configuration Web
		$rootScope.setConfiguration = function () {
			$rootScope.configuration.animations = $rootScope.storage.configuration.animations;
			$rootScope.configuration.theme_basic = $rootScope.storage.configuration.theme_basic;
			$rootScope.configuration.delay_notification = $rootScope.storage.configuration.delay_notification;
			$rootScope.configuration.delay_locked = $rootScope.storage.configuration.delay_locked;
			// Set timeout to locked screen
			if ($rootScope.configuration.delay_locked) {
				var timelocked = $rootScope.configuration.delay_locked;
			} else {
				var timelocked = DELAY_IDLE;
			}
			$idle._options().idleDuration = timelocked * 60;
			if ($rootScope.configuration.views.indexOf($rootScope.storage.configuration.view) !== -1) {
				$rootScope.configuration.view = $rootScope.storage.configuration.view;
			} else {
				$rootScope.configuration.view = 'grid';
			}
			if ($rootScope.configuration.viewsCalendar.indexOf($rootScope.storage.configuration.view_calendar) !== -1) {
				$rootScope.configuration.view_calendar = $rootScope.storage.configuration.view_calendar;
			} else {
				$rootScope.configuration.view_calendar = 'month';
			}
			less.modifyVars({
				'color-primary': $rootScope.storage.configuration.color_primary,
				'background-splash': $rootScope.storage.configuration.color_primary
			});
		};
		/*****************/
		/* FUNCTIONS */
		/*****************/
	}
	/*******************/
	/* END LOAD IF LOGGED  */
	/*******************/
});
/******************************* END APP CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

