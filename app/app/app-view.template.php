<?php // @include ('../../php/checkLogged.php');                     ?>


<div class="page-container" ng-class="{
			blur : $root.isBlur
		}">
	<div ng-if="isLocked == false">
		<div id="menu-view" ui-view="menu-view"></div>
		<div id="content-view" ui-view="content-view"></div>
	</div>
	<div ng-if="isLocked" ng-include="'app/app/app-locked.template.php'"></div>
</div>
