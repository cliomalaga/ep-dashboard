// To preview image Frontend
app.directive('ngFileModel', ['$parse', function ($parse) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				var model = $parse(attrs.ngFileModel);
				var isMultiple = attrs.multiple;
				var modelSetter = model.assign;
				element.bind('change', function () {
					var values = [];
					angular.forEach(element[0].files, function (item) {
						var value = {
							// File Name
							name: item.name,
							//File Size
							size: item.size,
							//File URL to view
							url: URL.createObjectURL(item),
							// File Input Value
							_file: item
						};
						values.push(value);
					});
					scope.$apply(function () {
						if (isMultiple) {
							modelSetter(scope, values);
						} else {
							modelSetter(scope, values[0]);
						}
					});
				});
			}
		};
	}
]);


// To upload image DB
app.directive("ngFileModel", [function () {
		return {
			scope: {
				ngFileModel: "="
			},
			link: function (scope, element, attributes) {
				element.bind("change", function (changeEvent) {
					var reader = new FileReader();
					reader.onload = function (loadEvent) {
						scope.$apply(function () {
							scope.ngFileModel = {
								name: changeEvent.target.files[0].name,
								size: changeEvent.target.files[0].size,
								type: changeEvent.target.files[0].type,
								data: loadEvent.target.result
							};
						});
					}
					if (changeEvent.target.files[0]) {
						reader.readAsDataURL(changeEvent.target.files[0]);
					}
				});
			}
		};
	}
]);

app.directive('forceboolean', function () {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModel) {

			//format text going to user (model to view)
			ngModel.$formatters.push(function (value) {
				if (parseInt(value) == 1) {
					return true;
				} else {
					return false;
				}
			});
		}
	}
});


app.directive('focusMe', function ($timeout) {
	return {
		link: function (scope, element, attrs) {
			scope.$watch(attrs.focusMe, function (value) {
				if (value === true) {
					//$timeout(function() {
					element[0].focus();
					scope[attrs.focusMe] = false;
					//});
				}
			});
		}
	};
});


app.directive('idle', function ($idle, $timeout, $interval) {
	return {
		restrict: 'A',
		link: function (scope, elem, attrs) {
			var timeout;
			var timestamp = localStorage.lastEventTime;

			// Watch for the events set in ng-idle's options
			// If any of them fire (considering 500ms debounce), update localStorage.lastEventTime with a current timestamp
			elem.on($idle._options().events, function () {
				if (timeout) {
					$timeout.cancel(timeout);
				}
				timeout = $timeout(function () {
					localStorage.setItem('lastEventTime', new Date().getTime());
				}, 500);
			});

			// Every 5s, poll localStorage.lastEventTime to see if its value is greater than the timestamp set for the last known event
			// If it is, reset the ng-idle timer and update the last known event timestamp to the value found in localStorage
			$interval(function () {
				if (localStorage.lastEventTime > timestamp) {
					$idle.watch();
					timestamp = localStorage.lastEventTime;
				}
			}, 1000);
		}
	};
});

app.directive('resizeimgresponsive', function ($timeout) {
	return {
		restrict: 'A',
		scope: {src: "@src"},
		link: function (scope, element, attributes) {
			scope.$watch("src", function (newValue, oldValue) {
				$timeout(function () {
					var width = element.width();
					var height = element.height();
					if (width > height) {
						element.removeClass('width-responsive');
						element.addClass('height-responsive');
					} else {
						element.removeClass('height-responsive');
						element.addClass('width-responsive');
					}
				}, 300);
			});
		}
	};
});

app.directive('resizeimgresponsiveinverse', function ($timeout) {
	return {
		restrict: 'A',
		scope: {src: "@src"},
		link: function (scope, element, attributes) {
			scope.$watch("src", function (newValue, oldValue) {
				$timeout(function () {
					var width = element.width();
					var height = element.height();
					if (width < height) {
						element.removeClass('width-responsive');
						element.addClass('height-responsive');
					} else {
						element.removeClass('height-responsive');
						element.addClass('width-responsive');
					}
				}, 300);
			});
		}
	};
});