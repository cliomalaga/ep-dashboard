<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			<span>
				{{ actualSection.name | uppercase }}
			</span>
			<div>
				<button type="button" ng-if="filters.show" ng-click="resetFilters()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-ccw"  data-title="Restaurar Filtros" alt="Restaurar Filtros" ></i>
				</button>
				<button type="button" ng-if="filters.show" ng-click="sortInverse()" class="press-button btn btn-item btn-primary-inverse">
					<i ng-if="filters.sortOrder" class="icon-arrow-up"  data-title="Orden Ascendente" alt="Orden Ascendente" ></i>
					<i ng-if="!filters.sortOrder" class="icon-arrow-down"  data-title="Orden Descendente" alt="Orden Descendente" ></i>
				</button>
				<span ng-if="filters.show">|</span>
				<button type="button" ng-click="toogleFilters()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-funnel" ng-if="!filters.show" data-title="Mostrar Filtros" alt="Mostrar Filtros" ></i>
					<i class="icon-funnel" ng-if="filters.show" data-title="Ocultar Filtros" alt="Ocultar Filtros" ></i>
				</button>
			</div>
		</h1>
		<hr>
	</div>
	<div class="content-filter" ng-if="filters.show">
		<div>
			<div class="tile-item-content form-primary" >
				<div class="form-entry">
					<input type="text" focus-me="filters.focus" class="form-control" name="name" ng-model="filters.search" placeholder="Buscar...">
				</div>
				<div class="form-entry">
					<select ng-model="filters.sortField" ng-options="k as v for (k, v) in filters.sortOptions">
						<option value="">Ordenar por...</option>
					</select>
				</div>
			</div>
		</div>
		<hr>
	</div>

	<div class="container-include">
		<div class="content-body table">
			<div class="row">
				<div class="no-results">
					<img ng-if="collection.length == 0" ng-src="assets/images/empty.png"/>
					<h2 ng-if="collection.length == 0">No existen {{actualSection.name}}</h2>
					<img ng-if="collection.length > 0" ng-hide="filteredItems.length" ng-src="assets/images/no-found.png"/>
					<h2 ng-if="collection.length > 0" ng-hide="filteredItems.length">No hay coincidencias</h2>
				</div>


				<div class="table table-bordered responsive col-xs-12" ng-hide="collection.length == 0 || filteredItems.length == 0">
					<div class="thead">
						<div class="tr">
							<div ng-if="isExistProperty(collection[0], 'subject')" class="th">Asunto</div>
							<div ng-if="isExistProperty(collection[0], 'messagge')" class="th">Mensaje</div>
							<div ng-if="isExistProperty(collection[0], 'created_by')" class="th td-hide-responsive-xs">Enviado por</div>
							<div class="th">Fecha</div>
							<div class="th"></div>
						</div>
					</div>
					<div class="tbody">
						<div ng-repeat="item in filteredItems = (collection| filter : searchItem | sortItemBy : filters.sortField : filters.sortOrder)"
							 class="item-active tr">
							<div ng-if="isExistProperty(item, 'subject')" class="td td-title">{{ item.subject}}</div>
							<div ng-if="isExistProperty(item, 'messagge_pretty')" class="td">{{ item.messagge_pretty}}</div>
							<div ng-if="isExistProperty(item, 'created_by')" class="td td-hide-responsive-xs">{{ item.created_by.name}} {{ item.created_by.lastname}}</div>
							<div class="td">{{ formatDate(item.created_at) | date: "dd/MM/yyyy"}}</div>
							<div class="td">
								<a class="button-delete press-button">
									<i class="entypo-eye" ng-click="edit(item)" data-title="Ver detalles" alt="Ver detalles"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>