/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("EmailsItemController", function ($rootScope, $scope, $stateParams, $location, App) {
	// If user is not superuser, user out
	$rootScope.checkAdmin();

	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'email',
		nameDB: 'emails',
		name: 'mensajes',
		nameUrl: 'mensajes',
		itemName: 'mensaje'
	};
	$rootScope.item = {};
	$scope.sendingMail = false;
	/******************/
	/* END VARIABLES  */
	/******************/



	/*******************/
	/* START ONLOAD */
	/*******************/
	// If item ID exist, the item is editing. If not exist ID, the item is new
	if ($stateParams.ID) {
		$rootScope.item.id = $stateParams.ID;
		$rootScope.readItem();
	} else {
		if ($rootScope.emailsSend && $rootScope.emailsSend.length) {
			delete $rootScope.item.id;
			$rootScope.item.emails = $rootScope.emailsSend;
			$rootScope.item.subject = '';
			$rootScope.item.messagge = '';
		} else {
			$rootScope.selectSection('mensajes/enviar');
			$location.path('mensajes/enviar');
		}
	}
	/*******************/
	/* END ONLOAD */
	/*******************/

	/*****************/
	/* START FUNCTIONS */
	/*****************/
	$scope.backSend = function () {
		$rootScope.selectSection('mensajes/enviar');
		$location.path('mensajes/enviar');
	};

	$scope.back = function () {
		$rootScope.selectSection('mensajes');
		$location.path('mensajes');
	};

	$scope.sendEmails = function () {
		$scope.sendingMail = true;
		var array = $rootScope.storage[$rootScope.actualSection.nameDB];
		App.sendEmails($rootScope.item).then(function (data) {
			// If request is success
			if (App.responseOK(data)) {
				// Create item in local
				array.unshift(data.response);
				$rootScope.item = angular.copy(data.response);
				$rootScope.itemOriginal = angular.copy(data.response);
				$rootScope.collection = angular.copy(array);
				$rootScope.selectSection('mensajes');
				$location.path('mensajes');
			}
			$scope.sendingMail = false;
		});
	};
	/*****************/
	/* END FUNCTIONS */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

