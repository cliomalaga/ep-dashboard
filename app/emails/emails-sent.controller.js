/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("EmailsSentController", function ($rootScope) {
	// If user is not superuser, user out
	$rootScope.checkAdmin();

	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		nameDB: 'mails',
		name: 'mensajes',
		nameUrl: 'mensajes',
		itemName: 'mensaje'
	};
	$rootScope.filters = {};
	$rootScope.mail = {};
	$rootScope.mail.arrayMails = [];
	$rootScope.filters.show = false;
	$rootScope.filters.sortOptions = {
		'name': 'Nombre',
		'lastname': 'Apellidos',
		'about': 'Sobre mi',
		'active': 'Activos',
		'created_at': 'Fecha de creación'
	};
	/******************/
	/* END VARIABLES  */
	/******************/



	/*******************/
	/* START ONLOAD */
	/*******************/
	$rootScope.resetFilters();
	$rootScope.readList();
	/*****************/
	/* END ONLOAD */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

