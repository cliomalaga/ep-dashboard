<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                                                        ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			<span>
				{{ actualSection.name | uppercase }}
			</span>
			<div>
				<button type="button" ng-if="filters.show" ng-click="resetFilters()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-ccw"  data-title="Restaurar Filtros" alt="Restaurar Filtros" ></i>
				</button>
				<button type="button" ng-if="filters.show" ng-click="sortInverse()" class="press-button btn btn-item btn-primary-inverse">
					<i ng-if="filters.sortOrder" class="icon-arrow-up"  data-title="Orden Ascendente" alt="Orden Ascendente" ></i>
					<i ng-if="!filters.sortOrder" class="icon-arrow-down"  data-title="Orden Descendente" alt="Orden Descendente" ></i>
				</button>
				<span ng-if="filters.show">|</span>
				<button type="button" ng-click="toogleFilters()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-funnel" ng-if="!filters.show" data-title="Mostrar Filtros" alt="Mostrar Filtros" ></i>
					<i class="icon-funnel" ng-if="filters.show" data-title="Ocultar Filtros" alt="Ocultar Filtros" ></i>
				</button>
				<button type="button" ng-click="selectAll()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-squared-cross" ng-if="!selectAllEmails" data-title="Seleccionar Todos" alt="Seleccionar Todos" ></i>
					<i class="icon-document" ng-if="selectAllEmails" data-title="Deseleccionar Todos" alt="Deseleccionar Todos" ></i>
				</button>
				<button type="button" ng-disabled="email.arrayEmails.length == 0" ng-click="writeEmail()" class="press-button btn btn-item btn-primary-inverse">
					<i class="entypo-mail" data-title="Escribir mensaje" alt="Escribir mensaje" ></i>
				</button>
			</div>
		</h1>
		<hr>
	</div>
	<div class="content-filter" ng-if="filters.show">
		<div>
			<div class="tile-item-content form-primary" >
				<div class="form-entry">
					<input type="text" focus-me="filters.focus" class="form-control" name="name" ng-model="filters.search" placeholder="Buscar...">
				</div>
				<div class="form-entry">
					<select ng-model="filters.sortField" ng-options="k as v for (k, v) in filters.sortOptions">
						<option value="">Ordenar por...</option>
					</select>
				</div>
			</div>
		</div>
		<hr>
	</div>

	<div class="container-include">
		<div class="content-body table">
			<div class="row">
				<div class="no-results">
					<img ng-if="collection.length == 0" ng-src="assets/images/empty.png"/>
					<h2 ng-if="collection.length == 0">No hay {{actualSection.name}}</h2>
					<img ng-if="collection.length > 0" ng-hide="filteredItems.length" ng-src="assets/images/no-found.png"/>
					<h2 ng-if="collection.length > 0" ng-hide="filteredItems.length">No hay coincidencias</h2>
				</div>


				<div class="table table-bordered responsive col-xs-12"
					 ng-hide="collection.length == 0 || filteredItems.length == 0">
					<div class="thead">
						<div class="tr">
							<div ng-if="isExistProperty(collection[0], 'name')" class="th">Email</div>
							<div ng-if="isExistProperty(collection[0], 'name')" class="th">Nombre</div>
							<div ng-if="isExistProperty(collection[0], 'lastname')" class="th">Apellidos</div>
							<div ng-if="isExistProperty(collection[0], 'about')" class="th">Sobre mi</div>
							<div class="th-selectAllth th">
								<!--								<a ng-click="selectAll()">
																	<span ng-if="!selectAllEmails">Seleccionar Todos</span>
																	<span ng-if="selectAllEmails">Deseleccionar Todos</span>
																</a>-->
							</div>
						</div>
					</div>
					<div class="tbody">
						<div ng-repeat="item in filteredItems = (collection| filter : searchItem | sortItemBy : filters.sortField : filters.sortOrder)"
							 ng-class="{'item-active':isActive(item)}" class="tr">
							<div ng-if="isExistProperty(collection[0], 'email')" class="td">{{ item.email}}</div>
							<div ng-if="isExistProperty(collection[0], 'name')" class="td">{{ item.name}}</div>
							<div ng-if="isExistProperty(collection[0], 'lastname')" class="td">{{ item.lastname}}</div>
							<div ng-if="isExistProperty(collection[0], 'about')" class="td">{{ item.about}}</div>
							<div class="td">
								<label for="selected{{$index}}" class="label-checkbox">
									<input type="checkbox" id="selected{{$index}}" name="selected" ng-model="item.selected">
									<div class="dark" ng-click="selectEmail(item)"></div>
								</label>
							</div>
						</div>
					</div>
				</div>
				<!--				<div class="post-table">
									<a ng-click="selectAll()">
										<span ng-if="!selectAllEmails">Seleccionar todos</span>
										<span ng-if="selectAllEmails">Deseleccionar todos</span>
									</a>
								</div>-->
			</div>
		</div>
	</div>
</div>