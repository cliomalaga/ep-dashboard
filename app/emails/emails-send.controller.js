/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("EmailsSendController", function ($rootScope, $scope, $location, $timeout) {
	// If user is not superuser, user out
	$rootScope.checkAdmin();

	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		nameDB: 'users',
		name: 'enviar mensaje',
		nameUrl: 'mensaje',
		itemName: 'mensaje'
	};
	$rootScope.filters = {};
	$rootScope.emailsSend = [];
	$rootScope.selectAllEmails = false;
	$rootScope.filters.show = false;
	$rootScope.filters.sortOptions = {
		'name': 'Nombre',
		'lastname': 'Apellidos',
		'about': 'Sobre mi',
		'active': 'Activos',
		'created_at': 'Fecha de creación'
	};
	/******************/
	/* END VARIABLES  */
	/******************/



	/*******************/
	/* START FUNCTIONS */
	/*******************/
	// ON LOAD
	$rootScope.resetFilters();
	$rootScope.readList();

	// FUNCTIONS
	$rootScope.selectAll = function () {
		var boolean = false;
		// If not selected all, we will select all emails
		if ($rootScope.selectAllEmails == false) {
			boolean = true;
		}
		// Change state for each item and add or splice of emails array
		for (var i = 0; i < $rootScope.collection.length; i++) {
			$rootScope.collection[i].selected = boolean;
			$rootScope.selectEmail($rootScope.collection[i]);
		}
		// Update select all emails status
		$rootScope.selectAllEmails = boolean;
	};


	$rootScope.selectEmail = function (_item) {
		$timeout(function () {
			if ((i = $rootScope.emailsSend.indexOf(_item.email)) !== -1) {
				if (_item.selected == false) {
					// Delete of array
					$rootScope.emailsSend.splice(i, 1);
					$rootScope.selectAllEmails = false;
				}
			} else if (_item.selected == true) {
				// Add to array
				$rootScope.emailsSend.push(_item.email);
				// Check if all emails are selected
				var selectAll = true;
				for (var i = 0; i < $rootScope.collection.length; i++) {
					if ($rootScope.collection[i].selected == false) {
						selectAll = false;
						break;
					}
				}
				// If all emails are selected
				if (selectAll) {
					$rootScope.selectAllEmails = true;
				}
			}
		});
	};


	$scope.writeEmail = function () {
		if ($rootScope.emailsSend.length > 0) {
			$location.path($rootScope.actualSection.nameUrl + '/');
		}
	};
	/*****************/
	/* END FUNCTIONS */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

