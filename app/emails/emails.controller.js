/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("EmailsController", function ($rootScope) {
	// If user is not admin, user out
	$rootScope.checkAdmin();

	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'email',
		nameDB: 'emails',
		name: 'mensajes enviados',
		nameUrl: 'mensajes',
		itemName: 'mensaje'
	};
	$rootScope.filters = {};
	$rootScope.filters.show = false;
	$rootScope.filters.sortOptions = {
		'subject': 'Asunto',
		'messagge': 'Mensaje',
		'created_at': 'Fecha de creación'
	};
	/******************/
	/* END VARIABLES  */
	/******************/



	/*******************/
	/* START ONLOAD */
	/*******************/
	$rootScope.resetFilters();
	$rootScope.readList();
	/*****************/
	/* END ONLOAD */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

