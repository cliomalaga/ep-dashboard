<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			<span>
				{{ actualSection.name | uppercase }}
			</span>
			<div>
				<button type="button" ng-if="item.id" ng-click="back()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-chevron-left" data-title="Volver" alt="Volver" ></i>
				</button>
				<button type="button" ng-if="!item.id" ng-click="backSend()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-chevron-left" data-title="Volver" alt="Volver" ></i>
				</button>
				<button type="button" ng-disabled="sendingMail" ng-if="!item.id" ng-click="sendEmails()" class="press-button btn btn-item btn-primary-inverse">
					<i class="entypo-export" data-title="Enviar mensaje" alt="Enviar mensaje" ></i>
				</button>
			</div>
		</h1>
		<hr>
	</div>



	<div class="content-body">
		<div class="tile-group">
			<div class="tile-item-content form-primary">
				<br>
				<div class="form-entry" ng-if="item.id">
					<label class="control-label">
						<i class="fa fa-fw fa-user"></i>&nbsp;
						Enviado por
					</label>
					<span>
						{{ item.created_by.name}} {{ item.created_by.lastname}}
					</span>
				</div>
				<br ng-if="item.id">
				<div class="form-entry">
					<label class="control-label" ng-if="!item.id">
						<i class="fa fa-fw fa-user"></i>&nbsp;
						Enviar a
					</label>
					<label class="control-label" ng-if="item.id">
						<i class="fa fa-fw fa-user"></i>&nbsp;
						Enviado a
					</label>
					<span class="label-tag" ng-repeat="i in item.emails | orderBy">
						{{i}}
						<!--If item is new and item has minimum one receptor-->
						<span class="delete-label" ng-if="!item.id && item.emails.length > 1" ng-click="deleteLabel(i, item.emails)">X</span>
					</span>
				</div>
				<br>
				<div class="form-entry">
					<label class="control-label">
						<i class="fa fa-fw fa-feed"></i>&nbsp;
						Asunto
					</label>
					<input type="text" name="subject" ng-model="item.subject" ng-disabled="!isOwn(item) || item.id" class="form-control">
				</div>
				<br>
				<div class="form-entry">
					<label class="control-label">
						<i class="fa fa-fw fa-envelope-o"></i>&nbsp;
						Mensaje
					</label>
					<div text-angular ng-model="item.messagge" ng-disabled="!isOwn(item) || item.id"></div>
				</div>
			</div>
		</div>
	</div>
</div>