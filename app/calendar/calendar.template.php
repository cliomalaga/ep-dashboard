<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			<span>
				{{ actualSection.name | uppercase }}
			</span>
			<div>
				<button type="button" ng-click="previous()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-chevron-left" data-title="Anterior" alt="Anterior" ></i>
				</button>
				<button type="button" ng-click="next()" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-chevron-right" data-title="Siguiente" alt="Siguiente" ></i>
				</button>
				<button type="button" ng-click="toToday()" class="fc-basicDay-button press-button btn btn-item btn-primary-inverse">
					<i class="entypo-calendar" data-title="Ir al día actual" alt="Ir al día actual"></i>
				</button>
				<button type="button" ng-click="toogleCalendar()" class="fc-basicDay-button press-button btn btn-item btn-primary-inverse">
					<i class="entypo-eye" data-title="Cambiar tipo calendario" alt="Cambiar tipo calendario" ></i>
				</button>
			</div>
		</h1>
		<hr>
	</div>



	<div class="content-body">

		<div class="tile-group">
			<div id="loading-calendar">
				<h1 >
					Creando el calendario...
				</h1>
				<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>
			</div>
			<div class="tile-item-content form-primary">








				<div id="calendar"></div>



			</div>
		</div>
	</div>
</div>