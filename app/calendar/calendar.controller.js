/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("CalendarController", function ($rootScope, $scope, $timeout) {
	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'event',
		nameDB: 'events',
		name: 'calendario',
		nameUrl: 'calendario',
		itemName: 'calendario'
	};
	/******************/
	/* END VARIABLES  */
	/******************/


	/*******************/
	/* START FUNCTIONS */
	/*******************/
	$scope.previous = function () {
		$('#calendar').fullCalendar('prev');
	};

	$scope.next = function () {
		$('#calendar').fullCalendar('next');
	};

	$scope.toToday = function () {
		$('#calendar').fullCalendar('today');
	};


	$rootScope.createCalendar = function () {
		$('#calendar').fullCalendar({
			header: {
				left: 'prev, next, today',
				center: 'title',
				right: 'month, agendaWeek, listWeek'
			},
			navLinkDayClick: function (date, jsEvent) {
				$('#calendar').fullCalendar('gotoDate', date);
				$('#calendar').fullCalendar('changeView', 'agendaDay');
				return false;
			},
			eventClick: function (calEvent, jsEvent, view) {
				return false;
			},
			eventAfterAllRender: function (view) {
				$('#calendar').addClass('calendar-loaded');
				return false;
			},
			locale: 'es',
			defaultDate: new Date(),
			slotLabelFormat: 'H(:mm)A',
			editable: false,
			defaultView: $rootScope.configuration.view_calendar,
			navLinks: true, // can click day/week names to navigate views
			eventLimit: true, // allow "more" link when too many events
			eventLimitClick: "week", // allow "more" link when too many events
			noEventsMessage: 'No hay eventos para mostrar',
			events: $rootScope.events
		});
	};
	/*****************/
	/* END FUNCTIONS */
	/*****************/


	/*******************/
	/* START ONLOAD */
	/*******************/
//	$rootScope.readList();

	if ($rootScope.events) {
		$rootScope.createCalendar();
	} else {
		$timeout(function () {
			$rootScope.createCalendar();
		}, 5000);
	}
	/*****************/
	/* END ONLOAD */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

