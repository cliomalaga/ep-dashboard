/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("ProfileController", function ($rootScope, App) {
	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'profiles',
		nameDB: 'profiles',
		name: 'perfil',
		nameUrl: 'perfil',
		itemName: 'perfil',
		currentTab: 'personal'
	};
	/******************/
	/* END VARIABLES  */
	/******************/


	/*******************/
	/* START ONLOAD */
	/*******************/
	// If current user ID exist, load profile to local else load profile to DB
	if ($rootScope.globals.currentUser.id) {
		$rootScope.item = {};
		$rootScope.item.id_user = $rootScope.globals.currentUser.id;
		$rootScope.readItem();
	} else {
		// Load item from DB
		App.read($rootScope.actualSection.itemDB, $rootScope.item).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.item = data.response;
				$rootScope.itemOriginal = data.response;
			}
		});
	}
	/*****************/
	/* END ONLOAD */
	/*****************/

	/*******************/
	/* START FUNCTIONS */
	/*******************/
	$rootScope.activeTab = function (_tab) {
		$rootScope.actualSection.currentTab = _tab;
	};
	/*****************/
	/* END FUNCTIONS */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

