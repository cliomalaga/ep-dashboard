/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("UsersItemController", function ($rootScope, $stateParams) {
	// If user is not admin, user out
	$rootScope.checkSuperUser();

	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'user',
		nameDB: 'users',
		name: 'usuarios',
		nameUrl: 'usuarios',
		itemName: 'usuario',
		currentTab: 'personal'
	};
	/******************/
	/* END VARIABLES  */
	/******************/



	/*******************/
	/* START ONLOAD */
	/*******************/
	// If item ID exist, the item is editing. If not exist ID, the item is new
	if ($stateParams.ID) {
		$rootScope.item.id = $stateParams.ID;
		$rootScope.readItem();
	} else {
		$rootScope.item = {};
		$rootScope.item.id_user = '';
		$rootScope.item.id_rol = '';
		$rootScope.item.pcurrent = '';
		$rootScope.item.pnew = '';
		$rootScope.item.pmatch = '';
		$rootScope.item.email = '';
		$rootScope.item.name = '';
		$rootScope.item.lastname = '';
		$rootScope.item.about = '';
		$rootScope.item.phone = '';
		$rootScope.item.avatar = '';
		$rootScope.item.Facebook = '';
		$rootScope.item.Twitter = '';
		$rootScope.item.Linkedin = '';
		$rootScope.item.Google = '';
		$rootScope.item.active = false;
	}
	/*****************/
	/* END ONLOAD */
	/*****************/

	/*******************/
	/* START FUNCTIONS */
	/*******************/
	$rootScope.activeTab = function (_tab) {
		$rootScope.actualSection.currentTab = _tab;
	};
	/*****************/
	/* END FUNCTIONS */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

