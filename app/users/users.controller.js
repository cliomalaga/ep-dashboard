/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("UsersController", function ($rootScope) {
	//	If user is not superuser, user out
	$rootScope.checkSuperUser();

	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'user',
		nameDB: 'users',
		name: 'usuarios',
		nameUrl: 'usuarios',
		itemName: 'usuario'
	};
	$rootScope.filters = {};
	$rootScope.filters.show = false;
	$rootScope.filters.sortOptions = {
		'name': 'Nombre',
		'lastname': 'Apellidos',
		'about': 'Sobre mi',
		'active': 'Activos',
		'created_at': 'Fecha de creación'
	};
	/******************/
	/* END VARIABLES  */
	/******************/



	/*******************/
	/* START ONLOAD */
	/*******************/
	// ON LOAD
	$rootScope.resetFilters();
	$rootScope.readList();
	/*****************/
	/* END ONLOAD */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

