<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                                ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			<span>
				{{ actualSection.name | uppercase }}
			</span>
			<div>
				<button type="button" ng-click="back(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-chevron-left" data-title="Volver" alt="Volver" ></i>
				</button>
				<button type="button" ng-click="undo(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-ccw" data-title="Deshacer" alt="Deshacer" ></i>
				</button>
				<button type="button" ng-click="save(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-save" data-title="Guardar" alt="Guardar" ></i>
				</button>
			</div>
		</h1>
		<hr>
	</div>



	<div class="content-body">

		<div class="tile-group">

			<div class="tile-image" ng-class="{'empty': !item.avatar && !item.images.url}">
				<img ng-if="item.avatar" class="image-item" ng-src="{{ item.avatar}}" ng-class="{
						'display-block' :  !isPreview(item)}"/>

				<img class="image-item preview" ng-src="{{ item.images.url}}" ng-class="{
						'display-block' :  isPreview(item)}"/>

				<p>
					<label for="image">
						<input type="file" id="image" name="image" ng-file-model="item.images" class="inputfile" ng-model="item.images"/>
						<i data-title="Cambiar imagen" alt="Cambiar imagen"></i>
					</label>
					<label ng-click="deleteImage(item)" ng-if="item.avatar || item.images">
						<i data-title="Eliminar imagen" alt="Eliminar imagen"></i>
					</label>
				</p>
			</div>

			<div class="tile-item-tab form-primary">
				<ul>
					<li ng-click="activeTab('personal')" ng-class="{
							'active': actualSection.currentTab == 'personal' }">Datos personales</li>
					<li ng-click="activeTab('rrss')" ng-class="{
							'active': actualSection.currentTab == 'rrss' }">Redes Sociales</li>
					<li ng-click="activeTab('admin')" ng-class="{
							'active': actualSection.currentTab == 'admin' }">Administración</li>
				</ul>
			</div>
			<div class="tile-item-content form-primary">
				<div ng-if="actualSection.currentTab == 'personal'" class="content-tab">
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-envelope-o"></i>&nbsp;
							Email
						</label>
						<input type="text" class="form-control" name="name" ng-model="item.email">
					</div>
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-user"></i>&nbsp;
							Nombre
						</label>
						<input type="text" class="form-control" name="name" ng-model="item.name">
					</div>
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-user"></i>&nbsp;
							Apellidos
						</label>
						<input type="text" class="form-control" name="lastname" ng-model="item.lastname">
					</div>
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-phone"></i>&nbsp;
							Teléfono
						</label>
						<input type="text" class="form-control" name="phone" ng-model="item.phone">
					</div>
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-info-circle"></i>&nbsp;
							Sobre mi
						</label>
						<textarea rows="3" name="about" ng-model="item.about" class="form-control" ></textarea>
					</div>
				</div>

				<div ng-if="actualSection.currentTab == 'rrss'" class="content-tab">
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-facebook"></i>&nbsp;
							Facebook
						</label>
						<input type="text" class="form-control" name="Facebook" ng-model="item.Facebook">
					</div>
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-twitter"></i>&nbsp;
							Twitter
						</label>
						<input type="text" class="form-control" name="Twitter" ng-model="item.Twitter">
					</div>
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-linkedin"></i>&nbsp;
							Linkedin
						</label>
						<input type="text" class="form-control" name="Linkedin" ng-model="item.Linkedin">
					</div>
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-google-plus"></i>&nbsp;
							Google+
						</label>
						<input type="text" class="form-control" name="Google" ng-model="item.Google">
					</div>
				</div>

				<div ng-if="actualSection.currentTab == 'admin'" class="content-admin">
					<div class="form-entry">
						<label class="control-label">
							<i class="icon icon-trophy"></i>&nbsp;
							Rol
						</label>
						<select ng-model="item.id_rol" name="id_rol" ng-options="option.level as option.name for option in storage.roles">
							<option value="">Seleccione un rol de usuario</option>
						</select>
					</div>
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-unlock-alt"></i>&nbsp;
							Nueva contraseña
						</label>
						<input type="password" class="form-control" name="pnew" ng-model="item.pnew">
						<label class="toogle-type-input" ng-click="toogleTypeInput($event)">
							<i class="fa fa-eye" data-title="Ver" alt="Ver"></i>
							<i class="fa fa-eye-slash" data-title="Ocultar" alt="Ocultar"></i>
						</label>
					</div>
					<div class="form-entry">
						<label class="control-label">
							<i class="fa fa-fw fa-unlock-alt"></i>&nbsp;
							Confirme contraseña
						</label>
						<input type="password" class="form-control" name="pmatch" ng-model="item.pmatch">
						<label class="toogle-type-input" ng-click="toogleTypeInput($event)">
							<i class="fa fa-eye" data-title="Ver" alt="Ver"></i>
							<i class="fa fa-eye-slash" data-title="Ocultar" alt="Ocultar"></i>
						</label>
					</div>
					<div ng-if="item.id != globals.currentUser.id" class="form-actions form-entry remember">
						<label class="control-label">
							<i class="fa fa-fw fa-check"></i>&nbsp;
							Activo
						</label>
						<label for="active" class="label-checkbox">
							<input type="checkbox" forceboolean id="active" name="active" ng-checked="isActive(item)" ng-model="item.active">
							<div></div>
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>