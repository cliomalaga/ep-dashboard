/*****************************************************************************************************************/
/*****************************************************************************************************************/
/******************************* START CONTROLLER ************************************************************/
app.controller("ConfigurationController", function ($rootScope, $scope, App) {
	/******************/
	/* VARIABLES      */
	/******************/
	$rootScope.actualSection = {
		itemDB: 'configuration',
		nameDB: 'configurations',
		name: 'configuración',
		nameUrl: 'configuracion',
		itemName: 'configuración'
	};
	/******************/
	/* END VARIABLES  */
	/******************/


	/*******************/
	/* START ONLOAD */
	/*******************/
	// If current user ID exist, load profile to local else load profile to DB
	if ($rootScope.storage['configurations']) {
		$rootScope.item = angular.copy($rootScope.storage['configurations'][0]);
		$rootScope.itemOriginal = angular.copy($rootScope.storage['configurations'][0]);
	} else {
		// Load item from DB
		App.read($rootScope.actualSection.itemDB, $rootScope.item).then(function (data) {
			if (App.responseOK(data)) {
				$rootScope.item = angular.copy(data.response);
				$rootScope.itemOriginal = angular.copy(data.response);
			}
		});
	}


	$scope.slider_options_notification = {
		floor: 0,
		ceil: 20,
		minLimit: 1,
		label: 'floor',
		translate: function (value, sliderId, label) {
			if (label == 'model') {
				return value + ' Segundos';
			}
			return value;
		}
	};


	$scope.slider_options_locked = {
		floor: 0,
		ceil: 60,
		minLimit: 10,
		label: 'floor',
		translate: function (value, sliderId, label) {
			if (label == 'model') {
				return value + ' Minutos';
			}
			return value;
		}
	};


	$scope.color_picker_settings = {
		position: 'top left',
		letterCase: 'uppercase',
		swatches: ['#303641', '#284239', '#2D4E54', '#3B455C', '#523B52', '#702D2D', '#9E6A2E',
			'#626366', '#4D804A', '#4B8A8A', '#4F7696', '#8E5B8F', '#CC4646', '#D1811F']
	};
	/*****************/
	/* END ONLOAD */
	/*****************/

	/*******************/
	/* START FUNCTIONS */
	/*******************/
	$rootScope.changeLess = function () {
		less.modifyVars({
			'color-primary': document.getElementById('color-input').value
		});
	};
	/*****************/
	/* END FUNCTIONS */
	/*****************/
});
/******************************* END CONTROLLER *******************************************************/
/*****************************************************************************************************************/
/*****************************************************************************************************************/

