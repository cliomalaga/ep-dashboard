<?php // @include ('../../php/checkLogged.php');                                                                                                                                                                                                                                                                                                                                                                                                                                       ?>


<div class="container-content">

	<div class="content-header">
		<h1 class="title-section">
			<span>
				{{ actualSection.name | uppercase }}
			</span>
			<div>
				<button type="button" ng-click="undo(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-ccw" data-title="Deshacer" alt="Deshacer" ></i>
				</button>
				<button type="button" ng-click="save(item)" class="press-button btn btn-item btn-primary-inverse">
					<i class="icon-save" data-title="Guardar" alt="Guardar" ></i>
				</button>
			</div>
		</h1>
		<hr>
	</div>



	<div class="content-body">

		<div class="tile-group">
			<div class="tile-item-content form-primary">
				<div class="form-entry">
					<label class="control-label">Animaciones <span>(OFF/ON)</span></label>
					<switch id="enabled" name="enabled" ng-model="item.animations" class="green"></switch>
				</div>

				<br>

				<div class="form-entry">
					<label class="control-label">Tema inverso / Tema básico</label>
					<switch id="enabled2" name="enabled2" ng-model="item.theme_basic" class="green"></switch>
				</div>

				<br>

				<div class="form-entry">
					<label class="control-label">Color primario <span>(RGB)</span></label>
					<input minicolors="color_picker_settings"
						   id="color-input"
						   class="form-control"
						   type="text"
						   ng-model="item.color_primary"
						   readonly="">
				</div>
				<br>

				<div class="form-entry configuration-views">
					<label class="control-label">Vista preferida</label>
					<button type="button" ng-class="{'active-view':item.view == 'grid'}" ng-click="item.view = 'grid'">
						<i class="entypo-layout"></i> Cuadrícula
					</button>
					<button type="button" ng-class="{'active-view':item.view == 'list'}" ng-click="item.view = 'list'">
						<i class="entypo-menu"></i> Listado
					</button>
					<button type="button" ng-class="{'active-view':item.view == 'table'}" ng-click="item.view = 'table'">
						<i class="entypo-window"></i> Tabla
					</button>
				</div>


				<div class="form-entry configuration-views">
					<label class="control-label">Vista preferida calendario</label>
					<button type="button" ng-class="{'active-view':item.view_calendar == 'month'}" ng-click="item.view_calendar = 'month'">
						<i class="entypo-calendar"></i> Mes
					</button>
					<button type="button" ng-class="{'active-view':item.view_calendar == 'agendaWeek'}" ng-click="item.view_calendar = 'agendaWeek'">
						<i class="icon-open-book"></i> Agenda
					</button>
					<button type="button" ng-class="{'active-view':item.view_calendar == 'listWeek'}" ng-click="item.view_calendar = 'listWeek'">
						<i class="icon-pin"></i> Listado
					</button>
				</div>

				<br>

				<div class="form-entry">
					<label class="control-label">Tiempo notificación <span>(Segundos)</span></label>
					<rzslider rz-slider-model="item.delay_notification"
							  rz-slider-options="slider_options_notification">
					</rzslider>
				</div>
				<div class="form-entry">
					<label class="control-label">Tiempo bloqueo <span>(Minutos)</span></label>
					<rzslider rz-slider-model="item.delay_locked"
							  rz-slider-options="slider_options_locked">
					</rzslider>
				</div>


			</div>
		</div>
	</div>
</div>