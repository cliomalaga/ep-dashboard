<?php

session_start();

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Content-Type: text/html; charset=utf-8');


$data = json_decode(file_get_contents("php://input"));
$action = $data->action;

if (isset($action)) {
      switch ($action) {
            /*             * ***************************************** Users ******************************************************* */
            case "create-session":
                  $_SESSION["userLogged"] = true;
                  break;
            case "delete-session":
                  unset($_SESSION["userLogged"]);
                  break;
      }
	  die();
}
