<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Content-Type: text/html; charset=utf-8');
//define("URL_API", "http://www.evaluacionpsicologica.es/EPAPI/public");
define("URL_API", "http://localhost/EP-API/public");
$data = json_decode(file_get_contents("php://input"));
$header = array(
	'Content-Type: application/x-www-form-urlencoded'
);
if (!empty($data->action)) {
	$action = $data->action;
}
if (!empty($data->token)) {
	$header[] = 'Authorization: ' . 'Bearer ' . $data->token;
}
if (!empty($data->item)) {
	$item = $data->item;
}

if (isset($action)) {
	switch ($action) {
		case "login":
			$method = 'POST';
			$url = '/authenticate';
			$header[] = 'email: ' . $item->email;
			$header[] = 'password: ' . $item->password;
			break;
		// ROLES
		case "read-roles":
			$method = 'GET';
			$url = '/roles';
			break;
		// PROFILES
		case "read-profiles":
			$method = 'GET';
			$url = '/perfil';
			break;
		case "update-profiles":
			$method = 'PUT';
			$url = '/perfil';
			break;
		// CATEGORIES
		case "read-categories":
			$method = 'GET';
			$url = '/categorias';
			break;
		// SUBCATEGORIES
		case "read-subcategories":
			$method = 'GET';
			$url = '/subcategorias';
			break;
		case "create-subcategories":
			$method = 'POST';
			$url = '/subcategorias';
			break;
		case "update-subcategories":
			$method = 'PUT';
			$url = '/subcategorias/' . $item->id;
			break;
		case "delete-subcategories":
			$method = 'DELETE';
			$url = '/subcategorias/' . $item->id;
			break;
		// TESTS
		case "read-tests":
			$method = 'GET';
			$url = '/pruebas';
			break;
		case "read-test":
			$method = 'GET';
			$url = '/pruebas/' . $item->id;
			break;
		case "create-tests":
			$method = 'POST';
			$url = '/pruebas';
			break;
		case "update-tests":
			$method = 'PUT';
			$url = '/pruebas/' . $item->id;
			break;
		case "delete-tests":
			$method = 'DELETE';
			$url = '/pruebas/' . $item->id;
			break;
		case "activate-tests":
			$method = 'GET';
			$url = '/pruebas/' . $item->id . '/activar';
			break;
		case "desactivate-tests":
			$method = 'GET';
			$url = '/pruebas/' . $item->id . '/desactivar';
			break;
		// FAQS
		case "read-faqs":
			$method = 'GET';
			$url = '/preguntas';
			break;
		case "read-faq":
			$method = 'GET';
			$url = '/preguntas/' . $item->id;
			break;
		case "create-faqs":
			$method = 'POST';
			$url = '/preguntas';
			break;
		case "update-faqs":
			$method = 'PUT';
			$url = '/preguntas/' . $item->id;
			break;
		case "delete-faqs":
			$method = 'DELETE';
			$url = '/preguntas/' . $item->id;
			break;
		case "activate-faqs":
			$method = 'GET';
			$url = '/preguntas/' . $item->id . '/activar';
			break;
		case "desactivate-faqs":
			$method = 'GET';
			$url = '/preguntas/' . $item->id . '/desactivar';
			break;
		// DOCUMENTS
		case "read-documents":
			$method = 'GET';
			$url = '/documentos';
			break;
		case "read-document":
			$method = 'GET';
			$url = '/documentos/' . $item->id;
			break;
		case "create-documents":
			$method = 'POST';
			$url = '/documentos';
			break;
		case "update-documents":
			$method = 'PUT';
			$url = '/documentos/' . $item->id;
			break;
		case "delete-documents":
			$method = 'DELETE';
			$url = '/documentos/' . $item->id;
			break;
		case "activate-documents":
			$method = 'GET';
			$url = '/documentos/' . $item->id . '/activar';
			break;
		case "desactivate-documents":
			$method = 'GET';
			$url = '/documentos/' . $item->id . '/desactivar';
			break;
		// TEACHINGS
		case "read-teachings":
			$method = 'GET';
			$url = '/ensenanzas';
			break;
		case "read-teaching":
			$method = 'GET';
			$url = '/ensenanzas/' . $item->id;
			break;
		case "create-teachings":
			$method = 'POST';
			$url = '/ensenanzas';
			break;
		case "update-teachings":
			$method = 'PUT';
			$url = '/ensenanzas/' . $item->id;
			break;
		case "delete-teachings":
			$method = 'DELETE';
			$url = '/ensenanzas/' . $item->id;
			break;
		case "activate-teachings":
			$method = 'GET';
			$url = '/ensenanzas/' . $item->id . '/activar';
			break;
		case "desactivate-teachings":
			$method = 'GET';
			$url = '/ensenanzas/' . $item->id . '/desactivar';
			break;
		// CASES
		case "read-cases":
			$method = 'GET';
			$url = '/casos';
			break;
		case "read-case":
			$method = 'GET';
			$url = '/casos/' . $item->id;
			break;
		case "create-cases":
			$method = 'POST';
			$url = '/casos';
			break;
		case "update-cases":
			$method = 'PUT';
			$url = '/casos/' . $item->id;
			break;
		case "delete-cases":
			$method = 'DELETE';
			$url = '/casos/' . $item->id;
			break;
		case "activate-cases":
			$method = 'GET';
			$url = '/casos/' . $item->id . '/activar';
			break;
		case "desactivate-cases":
			$method = 'GET';
			$url = '/casos/' . $item->id . '/desactivar';
			break;
		// INVESTIGATIONS
		case "read-investigations":
			$method = 'GET';
			$url = '/investigaciones';
			break;
		case "read-investigation":
			$method = 'GET';
			$url = '/investigaciones/' . $item->id;
			break;
		case "create-investigations":
			$method = 'POST';
			$url = '/investigaciones';
			break;
		case "update-investigations":
			$method = 'PUT';
			$url = '/investigaciones/' . $item->id;
			break;
		case "delete-investigations":
			$method = 'DELETE';
			$url = '/investigaciones/' . $item->id;
			break;
		case "activate-investigations":
			$method = 'GET';
			$url = '/investigaciones/' . $item->id . '/activar';
			break;
		case "desactivate-investigations":
			$method = 'GET';
			$url = '/investigaciones/' . $item->id . '/desactivar';
			break;
		// EVENTS
		case "read-events":
			$method = 'GET';
			$url = '/eventos';
			break;
		case "read-event":
			$method = 'GET';
			$url = '/eventos/' . $item->id;
			break;
		case "create-events":
			$method = 'POST';
			$url = '/eventos';
			break;
		case "update-events":
			$method = 'PUT';
			$url = '/eventos/' . $item->id;
			break;
		case "delete-events":
			$method = 'DELETE';
			$url = '/eventos/' . $item->id;
			break;
		case "activate-events":
			$method = 'GET';
			$url = '/eventos/' . $item->id . '/activar';
			break;
		case "desactivate-events":
			$method = 'GET';
			$url = '/eventos/' . $item->id . '/desactivar';
			break;
		// EMAILS
		case "read-emails":
			$method = 'GET';
			$url = '/mensajes';
			break;
		case "read-email":
			$method = 'GET';
			$url = '/mensajes/' . $item->id;
			break;
		case "send-emails":
			$method = 'POST';
			$url = '/mensajes/enviar';
			break;
		// USERS
		case "read-users":
			$method = 'GET';
			$url = '/usuarios';
			break;
		case "read-user":
			$method = 'GET';
			$url = '/usuarios/' . $item->id;
			break;
		case "create-users":
			$method = 'POST';
			$url = '/usuarios';
			break;
		case "update-users":
			$method = 'PUT';
			$url = '/usuarios/' . $item->id;
			break;
		case "delete-users":
			$method = 'DELETE';
			$url = '/usuarios/' . $item->id;
			break;
		case "activate-users":
			$method = 'GET';
			$url = '/usuarios/' . $item->id . '/activar';
			break;
		case "desactivate-users":
			$method = 'GET';
			$url = '/usuarios/' . $item->id . '/desactivar';
			break;
		// CONFIGURATIONS
		case "read-configuration":
			$method = 'GET';
			$url = '/configuracion';
			break;
		case "update-configurations":
			$method = 'PUT';
			$url = '/configuracion/' . $item->id;
			break;
		// LOGACTIVITIES
		case "read-logactivities":
			$method = 'GET';
			$url = '/logactividad';
			break;
	}
}

if (!empty($url)) {

	// Initialize session and set URL.
	$url = URL_API . $url;
	$ch = curl_init();
	$json = array();
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json);


	switch ($method) {
		case 'GET':
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			break;
		case 'POST':
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($item));
			break;
		case 'PUT':
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($item));
			break;
		case 'DELETE':
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			break;
	}

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	// Set so curl_exec returns the result instead of outputting it.
	$responseJson = curl_exec($ch);

//	die(print_r($responseJson));

	curl_close($ch);
	echo $responseJson;
} else {
	$myObj = new stdClass();
	$myObj->ok = false;
	$myObj->code = "500";
	$myObj->message = "Router-URL Not Found: " . $action;
	$myJSON = json_encode($myObj);
	echo $myJSON;
}