<?php

session_start();

// If not logged user, template is not rendered
if (!isLogged()) {
	exit();
}

function isLogged() {
	if (isset($_SESSION['userLogged'])) {
		return true;
	} else {
		return false;
	}
}
